package cn.lena.stringUtils;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

public class studyIsNotBlank {

	@Test
	public void testIsNotBlank(){
		String s1="";	//false
		String s2=" ";	//false
		String s3=null;	//false
		String s4=" lena ";	//true
		String s5="     lena";	//true
		System.out.println(StringUtils.isNotBlank(s1));
		System.out.println(StringUtils.isNotBlank(s2));
		System.out.println(StringUtils.isNotBlank(s3));
		System.out.println(StringUtils.isNotBlank(s4));
		System.out.println(StringUtils.isNotBlank(s5));
	}
}
