package cn.lena.stringUtils;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class studyReplaceEach {

	@Test
	public void testReplaceEach(){
		String[] urlCode=new String[]{"%5B","%5D"};
		String[] trueCode=new String[]{"[","]"};
		//"[你好]"的url编码格式
		String httpCode="%5B%E4%BD%A0%E5%A5%BD%5D";
		httpCode=StringUtils.replaceEach(httpCode,urlCode,trueCode);
		System.out.println(httpCode);	//输出==>[%E4%BD%A0%E5%A5%BD]
	}

	@Test
	public void testURLDecoder(){
		//"[你好]"的url编码格式
		String httpCode="%5B%E4%BD%A0%E5%A5%BD%5D";
		try {
			httpCode= URLDecoder.decode(httpCode,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.out.println(httpCode);		//输出==>[你好]
	}

	@Test
	public void testURLEncoder(){
		String httpCode="[你好]";
		try {
			httpCode= URLEncoder.encode(httpCode,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.out.println(httpCode);		//输出==>%5B%E4%BD%A0%E5%A5%BD%5D
	}

}
