package cn.lena.sort;

import org.junit.Test;

public class ChangeSortTest {

	//测试快速排序
	@Test
	public void test1(){
		int a[]={23,13,49,6,31,19,28};
		ChangeSort changeSort = new ChangeSort();
		int[] ints = changeSort.fastSort(a,0,6);
		for (int anInt : ints) {
			System.out.print(anInt);
		}
	}

	//测试快速排序
	@Test
	public void test2(){
		int a[]={23,13,49,6,31,19,28};
		ChangeSort changeSort = new ChangeSort();
		int[] ints = changeSort.fastSort(a,0,6);
		for (int anInt : ints) {
			System.out.print(anInt);
		}
	}

}
