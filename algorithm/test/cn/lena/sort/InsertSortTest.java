package cn.lena.sort;

import org.junit.Test;

public class InsertSortTest {

	//测试直接插入排序
	@Test
	public void test1(){
		int a[]={5,3,2,1};
		InsertSort insertSort = new InsertSort();
		int[] ints = insertSort.directInsertSort(a);
		for (int anInt : ints) {
			System.out.print(anInt);
		}
	}

	//测试折半插入排序
	@Test
	public void test2(){
		int a[]={5,1,3,2};
		InsertSort insertSort = new InsertSort();
		int[] ints = insertSort.halfInsertSort(a);
		for (int anInt : ints) {
			System.out.print(anInt);
		}
	}


}
