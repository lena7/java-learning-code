package cn.lena.sort;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

/**
 * @author lena
 * @date 2021/7/18
 */
public class test {


	@Test
	public void test22(){
		ArrayList<Integer> a=new ArrayList<>();
		a.add(1);a.add(2);a.add(3);
		Integer[] arr=new Integer[3];
		a.toArray(arr);
		for(int aas:arr){
			System.out.println(aas);
		}
		System.out.println(arr);

	}

	@Test
	public void test1(){
		int[][] arr={{1,3,1},{1,5,1},{4,2,1}};
		int min = findMin2(arr);
		System.out.println(min);
	}

	//t
	public int findMin2(int[][] mapArray) {
		int x = mapArray.length - 1;
		int y = mapArray[0].length - 1;
		// 更新最右的值
		for (int i = x-1; i >= 0; i--) {
			mapArray[i][y] += mapArray[i+1][y];
		}
		// 更新最下的值
		for(int i=y-1;i>=0;i--) {
			mapArray[x][i] += mapArray[x][i+1];
		}
		for(int i=y-1;i>=0;i--){
			for(int j=x-1;j>=0;j--){
				mapArray[i][j] += Math.min(mapArray[i+1][j],mapArray[i][j+1]);
				System.out.println(mapArray[i][j]);
			}
		}

		return mapArray[0][0];
	}

	// [[1,3,1],
	// [1,5,1],
	// [4,2,1]]
	// 输入矩阵，找到从左上角到右下角和最小的走法
	// 查找最小路径和
	public int findMin(int[][] mapArray) {
		// 贪心算法
		int i=0,j=0;
		int sum=mapArray[0][0];
		while(i != mapArray.length && j != mapArray[0].length) {
			if(i == mapArray.length-1 && j == mapArray[0].length-1){
				break;
			}
			if(i == mapArray.length-1) {
				// 只能向右走
				sum += mapArray[i][j++];
				continue;
			}
			if(j == mapArray[0].length-1) {
				// 只能向下走
				sum += mapArray[i++][j];
				continue;
			}
			if(mapArray[i+1][j] <= mapArray[i][j+1]) {
				// 向下走
				sum += mapArray[i++][j];
			}else{
				sum += mapArray[i][j++];
			}
		}
		return sum;
	}


	private int find(int[][] arr,int i,int j,int sum) {
		if(i == arr.length-1 && j == arr[0].length-1) {
			return sum;
		}
		if(i == arr.length-1) {
			// i已经指向最后一行：只能向右
			sum += arr[i][j++];
		}
		if(j == arr[0].length-1) {
			sum += arr[i++][j];
		}
		return 0;
	}

	@Test
	public void test(){
	//	String[][] arr={{"0.1.0", "1.0.2"}, {"2.1.13", "1.20.0"}, {"2.1.0", "2.1"},{"1.0","1.0.1"}};
		String[][] arr={{"1.0","1.0.1"}};
		int[] ints = max_version(arr);
		for(int a:ints) {
			System.out.println(a);
		}
	}

	// 比较版本号，版本号数值<1000
	public int[] max_version (String[][] version_list) {
		int[] res=new int[version_list.length];
		for(int i=0;i<res.length;i++) {
			String[] a=version_list[i][0].split("\\.");
			String[] b=version_list[i][1].split("\\.");
			for(int j=0;j<4;j++) {
				if(j == a.length && j == b.length) {
					res[i]=1;
					break;
				}else if(j == a.length) {
					// b数组还有数值
					if(Integer.valueOf(b[j]) > 0) {
						res[i]=2;
						break;
					}
				}else if(j == b.length){
					if(Integer.valueOf(a[j]) > 0) {
						res[i]=1;
						break;
					}
				}
				if(Integer.valueOf(a[j]) > Integer.valueOf(b[j])) {
					res[i]=1;
					break;
				}else if(Integer.valueOf(a[j]) < Integer.valueOf(b[j])) {
					res[i]=2;
					break;
				}
			}
		}
		return res;
	}

	/**
	 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
	 *
	 *
	 * @param timeSchedule string字符串ArrayList<ArrayList<>>
	 * @return int整型
	 */
	// 输入：[["10:00","12:00"],["03:00","11:30"],["11:30","14:00"]]
	// 输出：最多活动数


	// 10:45-11:30 肝副本
	private int countCurrent(ArrayList<ArrayList<String>> timeSchedule,int current) {
		int sum=0;
		// 用来计算24小时是否被占用,元素取值0~60,正数代表前往后数、负数代表后往前
		int[] time=new int[24];
		for(int i=current;i<timeSchedule.size();i++) {
			String[] start=timeSchedule.get(i).get(0).split(":");
			String[] end=timeSchedule.get(i).get(1).split(":");
			int hour=Integer.valueOf(end[0]);
			int min=Integer.valueOf(end[1]);
			// 11:20 -30,20
			if(time[hour] > min) {
				// 当前时间被占用
				continue;
			}
			if(time[hour]<0 && 60+time[hour]<min) {
				continue;
			}
			sum++;
			time[hour]=min;
		}
		return 2;
	}

	// 输入：[["10:00","12:00"],["03:00","11:30"],["11:30","14:00"]]
	// 输出：最多活动数
	public int countMaxActivity (ArrayList<ArrayList<String>> timeSchedule) {
		// 使用string数组存储起止时间
		String[][] timeS = new String[timeSchedule.size()][2];
		for (int i = 0; i < timeSchedule.size(); i ++) {
			// 第一列存储开始时间 第二列存储结束时间
			timeS[i][0] = timeSchedule.get(i).get(0);
			timeS[i][1] = timeSchedule.get(i).get(1);
		}
		if (timeS.length == 0) {
			return 0;
		}
		// 数组排序 规则:结束时间进行比较
		Arrays.sort(timeS, new Comparator<String[]>() {
			public int compare(String[] o1, String[] o2) {
				return o1[1].compareTo(o2[1]);
			}
		});
		// count:完成总任务数
		int count = 1;
		// 第一个任务(最早开始任务)的结束时间
		String end = timeS[0][1];
		for (int i = 1; i < timeS.length; i ++) {
			// 下一个任务的起始时间 大于 当前任务的结束时间:可以完成下一个任务
			if (timeS[i][0].compareTo(end) >= 0) {
				count ++;
				// 更新当前可完成任务的结束时间
				end = timeS[i][1];
			}
		}
		return count;
	}

	@Test
	public void test2(){
		ArrayList<ArrayList<String>> array=new ArrayList<>();
		ArrayList<String> a=new ArrayList<>();
		a.add("10:00");a.add("19:00");array.add(a);
		ArrayList<String> b=new ArrayList<>();
		b.add("03:00");b.add("06:30");array.add(b);
		ArrayList<String> c=new ArrayList<>();
		c.add("11:30");c.add("14:00");array.add(c);
		System.out.println(countMaxActivity(array));
	}

}
