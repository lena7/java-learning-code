package cn.lena.algorithm;

import org.junit.Test;

import java.util.Arrays;

//kmp算法测试
public class KmpTest {

	@Test
	public void testKmp(){
		String str1 = "BBC ABCDAB ABCDABCDABDE";
		String str2 = "ABCDABD";
		//String str2 = "BBC";

		int[] next = Kmp.kmpNext("ABCDABD"); //[0, 1, 2, 0]
		System.out.println("next=" + Arrays.toString(next));

		int index = Kmp.kmpSearch(str1, str2, next);
		System.out.println("index=" + index); // 15了
	}

	@Test
	public void testViolenceMatch(){
		//测试暴力匹配算法
		String str1 = "硅硅谷 尚硅谷你尚硅 尚硅谷你尚硅谷你尚硅你好";
		String str2 = "尚硅谷你尚硅你";
		System.out.println("index=" + Kmp.violenceMatch(str1,str2));
	}
}
