package cn.lena.algorithm;

import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;

public class GreedyTest {

	@Test
	public void test(){
		System.out.println(3/2);

	}

	@Test	//两地调度
	public void testTwoCitySchedCost(){
		int costs[][]={{10,430,400,30},{20,200,50,20}};
		System.out.println(Greedy.twoCitySchedCost(costs));		//290
	}


	@Test	//广播电台的选取
	public void testBroadcasting(){
		//创建广播电台,放入到Map
		HashMap<String, HashSet<String>> broadcasts = new HashMap<String, HashSet<String>>();
		//将各个电台放入到broadcasts
		HashSet<String> hashSet1 = new HashSet<String>();
		hashSet1.add("北京");
		hashSet1.add("上海");
		hashSet1.add("天津");

		HashSet<String> hashSet2 = new HashSet<String>();
		hashSet2.add("广州");
		hashSet2.add("北京");
		hashSet2.add("深圳");

		HashSet<String> hashSet3 = new HashSet<String>();
		hashSet3.add("成都");
		hashSet3.add("上海");
		hashSet3.add("杭州");


		HashSet<String> hashSet4 = new HashSet<String>();
		hashSet4.add("上海");
		hashSet4.add("天津");

		HashSet<String> hashSet5 = new HashSet<String>();
		hashSet5.add("杭州");
		hashSet5.add("大连");

		//加入到map
		broadcasts.put("K1", hashSet1);
		broadcasts.put("K2", hashSet2);
		broadcasts.put("K3", hashSet3);
		broadcasts.put("K4", hashSet4);
		broadcasts.put("K5", hashSet5);

		//allAreas 存放所有的地区
		HashSet<String> allAreas = new HashSet<String>();
		allAreas.add("北京");
		allAreas.add("上海");
		allAreas.add("天津");
		allAreas.add("广州");
		allAreas.add("深圳");
		allAreas.add("成都");
		allAreas.add("杭州");
		allAreas.add("大连");

		//调用方法
		System.out.println("得到的选择结果是" + Greedy.broadcasting(allAreas, broadcasts));//[K1,K2,K3,K5]
	}

}
