package cn.lena.algorithm;

import org.junit.Test;

import java.util.Arrays;

//分治算法
public class DivideTest {

	@Test
	public void testTwoSum(){
		int num[]={2,-1,-5,15};
		int target=10;
		int[] ints = Divide.twoSum(num, target);
		System.out.print(ints[0]+","+ints[1]);


	}

	@Test
	public void testNumMatchingSubseq(){
		String s="abcde";
		String words[]={"a","bb","acd","ace","","ae","kkk"};
		Divide.numMatchingSubseq(s,words);
	}

	//汉诺塔
	@Test
	public void testHanoi(){
		Divide.hanoi(5, 'A', 'B', 'C');
	}
	//归并排序
	@Test
	public void testMergeSort(){
		int []arr = {9,0,7,6,5,4,3,2,1,8};
		Divide.mergeSort(arr);
		System.out.println(Arrays.toString(arr));
	}

}
