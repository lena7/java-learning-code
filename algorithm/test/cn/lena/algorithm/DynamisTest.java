package cn.lena.algorithm;

import org.junit.Test;

public class DynamisTest {

	@Test	//数字三角形（记忆数组）测试
	public void testDigTalTriangleByRecursion(){
		int tri[][]={{7,0,0,0,0},{3,8,0,0,0},{8,1,0,0,0},{2,7,4,4,0},{4,5,2,6,5}};
		Dynamic.digTalTriangleByRecursion(0,0,tri);
	}

	@Test	//数字三角形 递推
	public void testDigTalTriangleByPushOptimize(){
		int tri[][]={{7,0,0,0,0},{3,8,0,0,0},{8,1,0,0,0},{2,7,4,4,0},{4,5,2,6,5}};
		System.out.println(Dynamic.digTalTriangleByPushOptimize(tri));
	}

	@Test	//数字三角形 递推
	public void testDigTalTriangleByPush(){
		int tri[][]={{7,0,0,0,0},{3,8,0,0,0},{8,1,0,0,0},{2,7,4,4,0},{4,5,2,6,5}};
		System.out.println(Dynamic.digTalTriangleByPush(0,0,tri));
	}

	@Test	//最大上升子序列
	public void testLongestUpSubsequence(){
		int sub[]={9,2,3,6,8,2,0,5,3,8,9,2,1};
		System.out.println(Dynamic.longestUpSubsequence(sub));
	}

	@Test	//最大公共子序列
	public void testLongestPublishSubsequence(){
		char s1[]={'a','b','c','f','b','c'};
		char s2[]={'a','b','f','c','a','b'};
		System.out.println(Dynamic.longestPublishSubsequence(s1,s2));
	}

	@Test	//背包问题测试
	public void testKnapsackProblem(){
		int[] w = {1, 4, 3};//物品的重量 从0开始
		int[] val = {1500, 3000, 2000}; //物品的价值 从0开始
		int m = 4; //背包的容量
		Dynamic.knapsackProblem(w,val,m);
	}

}
