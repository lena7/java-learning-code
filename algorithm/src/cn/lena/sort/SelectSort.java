package cn.lena.sort;

/**
 * lena
 * 选择排序
 * 2021-03-04
 */
public class SelectSort {

	/*
	 * t直接选择排序：每一次选择一个最小的数
	 * a ==> 需要排序的数组
	 */
	public int[] directSelectSort(int[] a){
		int min;	//存放当前最小值
		int sign;	//记录当前最小值的索引位。
		int tmp;	//用于交换数字。
		for(int i=0;i<a.length-1;i++){
			min=a[i];
			sign=i;
			for(int j=i;j<a.length;j++){
				if (a[j]<min){
					min=a[j];
					sign=j;
				}
			}
			//如果相等则不需要移动
			if(sign!=i){
				tmp=a[i];
				a[i]=a[sign];
				a[sign]=tmp;
			}
		}
		return a;
	}

	/*
	 * 堆排序
	 * a ==> 需要排序的数组
	 */
	public int[] heapSort(int[] a){

		return a;
	}

}
