package cn.lena.lanQiao.province;

import org.junit.Test;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

//蓝桥杯第十一届第二场省赛JAVA-B组
public class elevent {

	@Test	//子串分值和
	public void I(){
		//获取输入字符串
/*		Scanner sc=new Scanner(System.in);
		String s=sc.nextLine();*/
		String s="ababc";
		//获取字符串的长度
		int length=s.length();
		int count=0;	//存储和
		for(int i=0;i<length;i++){
			for (int j=i+1;j<=length;j++)
				count+=sumCurrent(s.substring(i,j));  //[i,j)
		}
		System.out.println(count);
	}
	int sumCurrent(String s){
		if (s.length()==1)	return 1;
		int count=0;
		Set<Character> set=new HashSet();	//用于比较数组是否一致
		char[] str = s.toCharArray();
		for (int i=0;i<str.length;i++){
			if (set.add(str[i]))
				count++;
		}
		return count;
	}


	@Test //单词分析
	public void G(){
	//public static void main(String[] args){
		//获取输入单词
		Scanner sc=new Scanner(System.in);
		String in=sc.nextLine();
		//将字符串转换成字符数组
		char[] ins=in.toCharArray();
		//存储每个字母出现的次数
		int[] num=new int[26];

		//存储当前字母出现最多的次数
		int max=0;
		//存储遍历中的字母
		int current=0;
		//遍历数组
		for(int i=0;i<ins.length;i++){
			//1.当前对应字母的num+1
			current=ins[i]-'a';
			num[current]++;
			//2.判断最大值是否大于max
			if (num[current]>max){
				max=num[current];
			}
		}
		//获取最大的单词（次序最小）
		for(int i=0;i<26;i++){
			if(num[i]==max){
				System.out.println(Character.toChars(i+'a'));
				break;
			}
		}
		System.out.println(max);
	}

	@Test //成绩分析
	public void F(){
		Scanner sc=new Scanner(System.in);
		int num=sc.nextInt();	//获取参与考试人数
		int current=0;
		int sum=0;int max=0;int min=0;
		while(sc.hasNextInt()){
			current=sc.nextInt();
			//判断数字是否合法：0~100
			if(current<0||current>100){
				System.out.println("有非法数字输入");
				return;
			}
			if(current>max)
				max=current;
			if (current<min)
				min=current;
			sum+=current;
		}
		System.out.println(max);
		System.out.println(min);
		//若在sum/num前面没有添加(float),则整型的sum/num会得到整型。
		float res=(float)Math.round((float)sum/num*100)/100;
		System.out.println(res);

	}

	@Test	//门牌制作
	public void A(){
		String s;
		int num=0;
		int temp;
		for(int i=1;i<=2020;i++){
			s=i+"";
			if (s.contains("2")){	//包含2才计算
				temp=i;
				while(temp!=0){
					if (temp%10==2)
						num++;
					temp/=10;
				}
			}
		}
		System.out.println(num);	//624
	}

	@Test	//蛇形填数
	public void C(){
		int[] arr=new int[20];
		arr[0]=1;
		int add=4;
		for(int i=1;i<20;i++){
			arr[i]=arr[i-1]+add;
			add+=4;
		}
		System.out.println(arr[19]);	//761
	}

}
