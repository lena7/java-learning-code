package cn.lena.disignPatternClass.Observer;

public interface Buyer {

	public void receive(double price);

}
