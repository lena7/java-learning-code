package cn.lena.disignPatternClass.Observer;

public class Main {

	public static void main(String[] args) {
		Buyer a=new SharesBuyer("A");
		Buyer b=new SharesBuyer("B");
		Buyer c=new SharesBuyer("C");
		ManagerSystem m=new SharesSystem();
		m.add(a);m.add(b);m.add(c);
		m.setPrice(4.53);
		m.informToBuyer();
	}

}
