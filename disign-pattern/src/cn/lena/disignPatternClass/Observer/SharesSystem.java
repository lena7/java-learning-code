package cn.lena.disignPatternClass.Observer;


public class SharesSystem extends ManagerSystem {

	@Override
	void informToBuyer() {
		System.out.println("变化幅度达到5%");
		for(Buyer b : list){
			b.receive(price);
		}
	}
}
