package cn.lena.disignPatternClass.Observer;

import java.util.ArrayList;

public abstract class ManagerSystem {

	ArrayList<Buyer> list=new ArrayList<>();
	double price;

	public void setPrice(double price) {
		this.price = price;
	}

	public double getPrice() {
		return price;
	}

	public void add(Buyer buyer){
		list.add(buyer);
	}

	public void remove(Buyer buyer){
		list.remove(buyer);
	}

	abstract void informToBuyer();

}
