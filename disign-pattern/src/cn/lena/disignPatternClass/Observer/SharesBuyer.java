package cn.lena.disignPatternClass.Observer;

public class SharesBuyer implements Buyer{

	private String name;

	SharesBuyer(String name){
		this.name=name;
	}

	@Override
	public void receive(double price) {
		System.out.println("股民"+name+"收到最新价格："+price);
	}

}
