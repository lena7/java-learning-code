package cn.lena.disignPatternClass.Iterator;

public interface Iterator {
	void previous();
	void next();
	boolean isLast();
	boolean isFirst();
	void setItem(int i);
	Object current();
}
