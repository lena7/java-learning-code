package cn.lena.disignPatternClass.Iterator;

public interface Goods {
	Iterator createIterator();
}
