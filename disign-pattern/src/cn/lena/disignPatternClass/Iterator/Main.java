package cn.lena.disignPatternClass.Iterator;

public class Main {

	public static void main(String[] args) {
		Goods goods=new MyGoods();
		Iterator i = goods.createIterator();
		while(!i.isLast()){
			System.out.print(i.current());
			i.next();
		}
		System.out.println(i.current());
		System.out.println("-------------");
		while(!i.isFirst()){
			System.out.print(i.current());
			i.previous();
		}
		System.out.println(i.current());
		System.out.println("-------------");
		i.setItem(5);
		System.out.print(i.current());

	}

}
