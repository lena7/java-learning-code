package cn.lena.disignPatternClass.Iterator;

public class MyGoods implements Goods{

	private String[] goods={"A","B","C","D","E","F","G","H","I","J","K","L","M","N"};

	public void setGoods(String[] goods) {
		this.goods = goods;
	}

	@Override
	public Iterator createIterator() {
		return new MyIterator();
	}


	class MyIterator implements Iterator {

		private int index=0;

		@Override
		public void previous() {
			if (!isFirst())
				index--;
		}

		@Override
		public void next() {
			if(!isLast())
				index++;
		}

		@Override
		public boolean isLast() {
			return index==goods.length-1;
		}

		@Override
		public boolean isFirst() {
			return index==0;
		}

		@Override
		public void setItem(int i) {
			if (i>0&&i<goods.length){
				index=i;
			}
		}

		@Override
		public Object current() {
			return goods[index];
		}

	}
}

