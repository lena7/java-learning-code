package cn.lena.disignPatternClass.Facade;

public class MainFrame {
	private Memory m;
	private CPU c;
	private HandDisk hd;
	private OS os;

	public MainFrame(){
		m=new Memory();
		c=new CPU();
		hd=new HandDisk();
		os=new OS();
	}

	public void on(){
		m.check();
		c.run();
		hd.read();
		os.load();
	}

}
