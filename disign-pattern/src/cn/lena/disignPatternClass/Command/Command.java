package cn.lena.disignPatternClass.Command;

public abstract class Command {

	abstract void execute();

}
