package cn.lena.disignPatternClass.Command;

public class OpenCommand extends Command {

	private BoardScreen bs;

	public OpenCommand(){
		bs=new BoardScreen();
	}

	@Override
	public void execute() {
		bs.open();
	}
}