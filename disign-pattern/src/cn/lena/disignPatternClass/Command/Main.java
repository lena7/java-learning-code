package cn.lena.disignPatternClass.Command;

public class Main {
	public static void main(String[] args) {
		Menu m=new Menu(new OpenCommand(),new EditCommit(),new CreateCommand());
		m.create();
		m.edit();
		m.open();
	}
}
