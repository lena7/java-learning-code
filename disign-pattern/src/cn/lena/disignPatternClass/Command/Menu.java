package cn.lena.disignPatternClass.Command;

public class Menu {

	private Command openCommand;
	private Command editCommand;
	private Command createCommand;

	public Menu(OpenCommand openCommand,EditCommit editCommand,CreateCommand createCommand){
		this.openCommand=openCommand;
		this.editCommand=editCommand;
		this.createCommand=createCommand;
	}

	public void open(){
		openCommand.execute();
	}

	public void edit(){
		editCommand.execute();
	}

	public void create(){
		createCommand.execute();
	}

}
