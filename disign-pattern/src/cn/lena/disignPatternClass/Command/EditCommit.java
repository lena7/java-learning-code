package cn.lena.disignPatternClass.Command;

public class EditCommit extends Command {

	private BoardScreen bs;

	public EditCommit(){
		bs=new BoardScreen();
	}

	@Override
	public void execute() {
		bs.edit();
	}
}
