package cn.lena.disignPatternClass.Command;

public class CreateCommand extends Command {

	private BoardScreen bs;

	public CreateCommand(){
		bs=new BoardScreen();
	}

	@Override
	public void execute() {
		bs.create();
	}
}