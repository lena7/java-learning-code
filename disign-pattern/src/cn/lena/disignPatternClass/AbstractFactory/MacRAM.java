package cn.lena.disignPatternClass.AbstractFactory;

public class MacRAM implements RAM{
	@Override
	public void produce() {
		System.out.println("MacRAM");
	}
}
