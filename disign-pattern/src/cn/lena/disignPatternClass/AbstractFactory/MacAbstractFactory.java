package cn.lena.disignPatternClass.AbstractFactory;

public class MacAbstractFactory extends AbstractFactory {
	@Override
	RAM produceRAM() {
		return new MacRAM();
	}

	@Override
	CPU produceCPU() {
		return new MacCPU();
	}
}
