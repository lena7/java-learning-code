package cn.lena.disignPatternClass.AbstractFactory;

public class Main {
	public static void main(String[] args) {
		AbstractFactory ab=null;
		ab=new MacAbstractFactory();
		RAM ram = ab.produceRAM();
		ram.produce();
		CPU cpu = ab.produceCPU();
		cpu.produce();

		ab=new PcAbstractFactory();
		RAM ram1 = ab.produceRAM();
		ram1.produce();
		CPU cpu1 = ab.produceCPU();
		cpu1.produce();
	}
}
