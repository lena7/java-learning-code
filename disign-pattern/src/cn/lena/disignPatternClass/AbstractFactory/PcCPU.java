package cn.lena.disignPatternClass.AbstractFactory;

public class PcCPU implements CPU {
	@Override
	public void produce() {
		System.out.println("PcCPU");
	}
}
