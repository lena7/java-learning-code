package cn.lena.disignPatternClass.AbstractFactory;

public class PcAbstractFactory extends AbstractFactory {
	@Override
	RAM produceRAM() {
		return new PcRAM();
	}

	@Override
	CPU produceCPU() {
		return new PcCPU();
	}
}
