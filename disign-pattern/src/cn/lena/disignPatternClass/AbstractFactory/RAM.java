package cn.lena.disignPatternClass.AbstractFactory;

public interface RAM {
	void produce();
}
