package cn.lena.disignPatternClass.AbstractFactory;

public class PcRAM implements RAM {
	@Override
	public void produce() {
		System.out.println("PcRAM");
	}
}
