package cn.lena.disignPatternClass.AbstractFactory;

public interface CPU {
	void produce();
}
