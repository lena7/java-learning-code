package cn.lena.disignPatternClass.AbstractFactory;

public abstract class AbstractFactory {

	abstract RAM produceRAM();

	abstract CPU produceCPU();

}
