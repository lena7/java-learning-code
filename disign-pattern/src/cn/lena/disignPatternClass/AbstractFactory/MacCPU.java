package cn.lena.disignPatternClass.AbstractFactory;

public class MacCPU implements CPU {
	@Override
	public void produce() {
		System.out.println("MacCPU");
	}
}
