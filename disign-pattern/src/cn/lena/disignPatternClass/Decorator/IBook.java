package cn.lena.disignPatternClass.Decorator;

public abstract class IBook {

	private String bookName;

	abstract void borrowBook();

	abstract void returnBook();

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

}
