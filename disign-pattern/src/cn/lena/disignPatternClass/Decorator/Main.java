package cn.lena.disignPatternClass.Decorator;

public class Main {

	public static void main(String[] args) {
		IBook book=new Book("《设计模式》");
		book.borrowBook();
		book.returnBook();
		System.out.println("");
		NewBook b1=new NewBook(book);
		b1.borrowBook();
		b1.lose();
		b1.freeze();
		b1.returnBook();
	}

}
