package cn.lena.disignPatternClass.Decorator;

public class Book extends IBook {

	private String bookName;

	public Book(String bookName){
		this.bookName=bookName;
	}

	@Override
	public void borrowBook() {
		System.out.println(bookName+"被借了");
	}

	@Override
	public void returnBook() {
		System.out.println(bookName+"归还了");
	}

	@Override
	public String getBookName() {
		return bookName;
	}

	@Override
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
}

