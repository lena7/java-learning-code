package cn.lena.disignPatternClass.Decorator;

public class Changer extends IBook {

	IBook book;
	private String bookName;

	public Changer(IBook book){
		this.book=book;
	}

	@Override
	public void borrowBook() {
		System.out.println(bookName+"被借了");
	}

	@Override
	public void returnBook() {
		System.out.println(bookName+"归还了");
	}

}
