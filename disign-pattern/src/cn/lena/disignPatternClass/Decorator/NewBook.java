package cn.lena.disignPatternClass.Decorator;

public class NewBook extends Changer {

	public NewBook(IBook book){
		super(book);
	}

	public void freeze(){
		System.out.println(book.getBookName()+"冻结了");
	}

	public void lose(){
		System.out.println(book.getBookName()+"丢失了");
	}

	@Override
	public void borrowBook() {
		System.out.println(book.getBookName()+"被借了");
	}

	@Override
	public void returnBook() {
		System.out.println(book.getBookName()+"归还了");
	}

}
