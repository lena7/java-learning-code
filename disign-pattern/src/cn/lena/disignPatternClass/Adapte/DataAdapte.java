package cn.lena.disignPatternClass.Adapte;

public class DataAdapte extends DataOperation {

	private BinarySearch binarySearch;
	private QuickSort quickSort;

	public DataAdapte(){
		binarySearch=new BinarySearch();
		quickSort=new QuickSort();
	}

	@Override
	public void sort(int[] arr) {
		quickSort.quickSort(arr);
	}

	@Override
	public int search(int[] arr,int num) {
		return binarySearch.binarySearch(arr,num);
	}

}
