package cn.lena.disignPatternClass.Adapte;

public class Main {

	public static void main(String[] args) {
		DataOperation dop=new DataAdapte();
		int arr[]={5,23,12,10,3,13,9,0,7};
		dop.sort(arr);
		for(int a:arr){
			System.out.print(a+" ");
		}
		System.out.println();
		int search = dop.search(arr, 12);
		System.out.println(search);
	}

}
