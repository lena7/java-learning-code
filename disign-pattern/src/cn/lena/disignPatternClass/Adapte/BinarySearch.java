package cn.lena.disignPatternClass.Adapte;

public class BinarySearch {

	int binarySearch(int[] arr,int num){
		int i=0;
		int j=arr.length;
		while(i<=j){
			int mid=(i+j)/2;
			if (arr[mid]==num){
				return mid;
			}else if (arr[mid]>num){
				j=mid-1;
			}else{
				i=mid+1;
			}
		}
		return -1;
	}

}
