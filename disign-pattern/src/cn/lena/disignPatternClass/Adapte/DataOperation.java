package cn.lena.disignPatternClass.Adapte;

public abstract class DataOperation {
	abstract void sort(int[] arr);
	abstract int search(int[] arr,int num);
}
