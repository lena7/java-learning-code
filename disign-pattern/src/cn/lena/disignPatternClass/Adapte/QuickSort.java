package cn.lena.disignPatternClass.Adapte;

//快速排序
public class QuickSort{

	public void quickSort(int[] arr) {
		if (arr.length<=1)
			return;
		quick(arr,0,arr.length-1);
	}

	private void quick(int[] arr,int low,int high){
		if (low>high) return;
		int i=low;int j=high;
		int sign=arr[low];
		while(i!=j){
			while(i<j&&arr[j]>=sign){
				j--;
			}
			if (i<j){
				arr[i]=arr[j];
				i++;
			}
			while(i<j&&arr[i]<=sign){
				i++;
			}
			if(i<j){
				arr[j]=arr[i];
				j--;
			}
		}
		arr[i]=sign;
		quick(arr,low,i-1);
		quick(arr,i+1,high);
	}
}
