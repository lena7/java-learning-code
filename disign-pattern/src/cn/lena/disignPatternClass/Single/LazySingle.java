package cn.lena.disignPatternClass.Single;

public class LazySingle {

	private volatile static LazySingle instance;

	public static LazySingle getInstance(){
		if(instance==null){
			synchronized (LazySingle.class){
				if (instance==null){
					instance=new LazySingle();
				}
			}
		}
		return instance;
	}

}
