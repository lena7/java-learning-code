package cn.lena.disignPatternClass.Single;

public class Singleton {

	private static class SingleStatic{
		private static final Singleton instance=new Singleton();
	}

	public static final Singleton getInstance(){
		return SingleStatic.instance;
	}

}
