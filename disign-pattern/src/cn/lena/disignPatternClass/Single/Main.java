package cn.lena.disignPatternClass.Single;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		while(sc.hasNextLine()){
			sc.nextInt();
			LazySingle.getInstance();
		}
	}

}
