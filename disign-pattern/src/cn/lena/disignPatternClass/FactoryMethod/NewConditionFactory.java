package cn.lena.disignPatternClass.FactoryMethod;

public class NewConditionFactory extends ConditionFactory {

	@Override
	Condition produce() {
		return new NewCondition();
	}

}