package cn.lena.disignPatternClass.FactoryMethod;

public abstract class ConditionFactory {
	abstract Condition produce();
}
