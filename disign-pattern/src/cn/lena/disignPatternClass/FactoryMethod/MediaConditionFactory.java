package cn.lena.disignPatternClass.FactoryMethod;

public class MediaConditionFactory extends ConditionFactory {

	@Override
	Condition produce() {
		return new MediaCondition();
	}

}
