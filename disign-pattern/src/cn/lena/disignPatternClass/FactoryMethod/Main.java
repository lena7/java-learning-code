package cn.lena.disignPatternClass.FactoryMethod;

public class Main {

	public static void main(String[] args) {
		ConditionFactory conditionFactory =new HaierConditionFactory();
		Condition Haier = conditionFactory.produce();
		Haier.play();

		conditionFactory =new MediaConditionFactory();
		Condition Media = conditionFactory.produce();
		Media.play();

		conditionFactory=new NewConditionFactory();
		Condition newCondition = conditionFactory.produce();
		newCondition.play();

	}

}
