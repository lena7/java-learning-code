package cn.lena.disignPatternClass.FactoryMethod;

public class HaierConditionFactory extends ConditionFactory {

	@Override
	Condition produce() {
		return new HaierCondition();
	}

}

