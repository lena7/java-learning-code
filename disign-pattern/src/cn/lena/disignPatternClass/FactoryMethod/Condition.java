package cn.lena.disignPatternClass.FactoryMethod;

public abstract class Condition {
	abstract void play();
}
