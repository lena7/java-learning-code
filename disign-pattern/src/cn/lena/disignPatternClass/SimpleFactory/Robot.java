package cn.lena.disignPatternClass.SimpleFactory;

public class Robot extends Person {
	@Override
	void born() {
		System.out.println("robot");
	}
}
