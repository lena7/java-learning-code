package cn.lena.disignPatternClass.SimpleFactory;

public class Main {
	public static void main(String[] args) {
		Nvwa nvwa = new Nvwa();
		Person m = nvwa.produce("M");
		m.born();
		Person w = nvwa.produce("W");
		w.born();
		Person r = nvwa.produce("R");
		r.born();

//		Scanner sc=new Scanner(System.in);
//		String s = sc.nextLine();
//		Nvwa nvwa = new Nvwa();
//		Person produce = nvwa.produce(s);
//		produce.born();

	}
}
