package cn.lena.disignPatternClass.SimpleFactory;

public class Man extends Person {
	@Override
	void born() {
		System.out.println("man");
	}
}
