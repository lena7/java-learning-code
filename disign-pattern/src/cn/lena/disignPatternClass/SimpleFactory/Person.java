package cn.lena.disignPatternClass.SimpleFactory;

public abstract class Person {
	abstract void born();
}
