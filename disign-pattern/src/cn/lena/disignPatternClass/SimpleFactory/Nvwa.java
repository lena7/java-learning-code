package cn.lena.disignPatternClass.SimpleFactory;

public class Nvwa {

/*	Person produce(String str){
		if (str.equals("W")){
			return new Woman();
		}else if (str.equals("M")){
			return new Man();
		}
		return null;
	}*/
	Person produce(String str){
		if (str.equals("W")){
			return new Woman();
		}else if (str.equals("M")){
			return new Man();
		}else if(str.equals("R")){
			return new Robot();
		}
		return null;
	}

}
