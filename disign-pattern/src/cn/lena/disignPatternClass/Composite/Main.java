package cn.lena.disignPatternClass.Composite;

public class Main {

	public static void main(String[] args) {
		TextFile t1=new TextFile("A.txt");
		TextFile t2=new TextFile("B.txt");
		ImageFile i1=new ImageFile("A.png");
		MediaFile m1=new MediaFile("A.avi");
		MediaFile m2=new MediaFile("B.avi");
		MediaFile m3=new MediaFile("C.avi");
		Folder f1=new Folder("A");
		Folder f2=new Folder("B");
		f1.add(t1);f1.add(m1);f1.add(m3);f1.add(f2);
		f2.add(i1);f2.add(t2);f2.add(m2);
		f1.antivirus();
	}

}
