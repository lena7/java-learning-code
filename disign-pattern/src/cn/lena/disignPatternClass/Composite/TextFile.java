package cn.lena.disignPatternClass.Composite;

public class TextFile extends File {

	private String fileName;

	public TextFile(String fileName){
		this.fileName=fileName;
	}

	@Override
	void add(File file) {
		System.out.println("文件不能执行此操作");
	}

	@Override
	void remove(File file) {
		System.out.println("文件不能执行此操作");
	}

	@Override
	void antivirus() {
		System.out.println("文本文件："+fileName+"正在进行杀毒");
	}
}
