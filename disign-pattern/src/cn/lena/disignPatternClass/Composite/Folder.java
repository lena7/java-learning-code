package cn.lena.disignPatternClass.Composite;

import java.util.ArrayList;

public class Folder extends File {

	private ArrayList<File> list;
	private String folderName;

	public Folder(String name){
		list=new ArrayList<>();
		this.folderName=name;
	}

	@Override
	void add(File file) {
		list.add(file);
	}

	@Override
	void remove(File file) {
		list.remove(file);
	}

	@Override
	void antivirus() {
		for(File file:list){
			file.antivirus();
		}
	}
}
