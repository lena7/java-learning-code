package cn.lena.disignPatternClass.Composite;

public abstract class File {
	abstract void add(File file);
	abstract void remove(File file);
	abstract void antivirus();
}
