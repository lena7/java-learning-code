package cn.lena.disignPatternClass.Strategy;

public class Main {
	public static void main(String[] args) {
		User u=new User();
		u.setBook(new ComputerBook());
		System.out.println(u.discount(232.99));

		u.setBook(new LanguageBook());
		System.out.println(u.discount(232.99));

		u.setBook(new NovelBook());
		System.out.println(u.discount(232.99));
	}
}
