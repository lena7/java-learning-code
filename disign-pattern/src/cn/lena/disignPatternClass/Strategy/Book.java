package cn.lena.disignPatternClass.Strategy;

public abstract class Book {
	abstract Double discount(Double price);
}
