package cn.lena.disignPatternClass.Strategy;

public class User {

	private Book book;

	public void setBook(Book book) {
		this.book = book;
	}

	public Double discount(Double price){
		return book.discount(price);
	}

}
