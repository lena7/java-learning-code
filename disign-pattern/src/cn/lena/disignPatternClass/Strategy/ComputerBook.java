package cn.lena.disignPatternClass.Strategy;

public class ComputerBook extends Book {
	@Override
	Double discount(Double price) {
		return 0.9*price;
	}
}
