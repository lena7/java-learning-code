package cn.lena.disignPatternClass.Strategy;

public class NovelBook extends Book {
	@Override
	Double discount(Double price) {
		return price-((int)(price/100))*10;
	}
}
