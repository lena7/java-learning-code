package cn.lena.disignPatternClass.Strategy;

public class LanguageBook extends Book {
	@Override
	Double discount(Double price) {
		return price-2;
	}
}