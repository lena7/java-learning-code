package cn.lena.disignPattern.Decorator;

public class Ham extends Seasoning{
	Ham(Cake cake) {
		super(cake);
	}
	int cost(){
		return super.cost()+2;
	}
}
