package cn.lena.disignPattern.Decorator;

public class Egg extends Seasoning {
	Egg(Cake cake) {
		super(cake);
	}
	int cost(){
		return super.cost()+2;
	}
}
