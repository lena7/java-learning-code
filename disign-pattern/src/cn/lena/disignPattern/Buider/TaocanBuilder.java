package cn.lena.disignPattern.Buider;

public abstract class TaocanBuilder {

	protected Taocan taocan=new Taocan();

	public abstract void buildFood();

	public abstract void buildDrink();

	public abstract void buidDesserts();

	public Taocan getTaocan() {
		return taocan;
	}
}
