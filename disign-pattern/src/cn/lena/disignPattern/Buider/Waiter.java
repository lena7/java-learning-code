package cn.lena.disignPattern.Buider;

public class Waiter {

	private TaocanBuilder taocanBuilder;

	public void setTaocanBuilder(TaocanBuilder taocanBuilder) {
		this.taocanBuilder = taocanBuilder;
	}

	public Taocan prepare(){
		taocanBuilder.buildFood();
		taocanBuilder.buildDrink();
		taocanBuilder.buidDesserts();
		return taocanBuilder.getTaocan();
	}
}
