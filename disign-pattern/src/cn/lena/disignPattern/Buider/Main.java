package cn.lena.disignPattern.Buider;

public class Main {
	public static void main(String[] args) {
		TaocanBuilder tba=new TaocanBuilderA();
		Waiter w=new Waiter();
		w.setTaocanBuilder(tba);
		w.prepare();
		Taocan tca = tba.getTaocan();
		System.out.println(tba.getTaocan());

		TaocanBuilder tbb=new TaocanBuilderB();
		Waiter wb=new Waiter();
		wb.setTaocanBuilder(tbb);
		wb.prepare();
		System.out.println(tbb.getTaocan());
	}
}
