package cn.lena.disignPattern.Buider;

public class Taocan {

	private String food;
	private String drink;
	private String desserts;

	@Override
	public String toString() {
		return getFood()+"+"+getDrink()+"+"+getDesserts();
	}

	public String getFood() {
		return food;
	}

	public void setFood(String food) {
		this.food = food;
	}

	public String getDrink() {
		return drink;
	}

	public void setDrink(String drink) {
		this.drink = drink;
	}

	public String getDesserts() {
		return desserts;
	}

	public void setDesserts(String desserts) {
		this.desserts = desserts;
	}
}
