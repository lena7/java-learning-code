package cn.lena.disignPattern.Buider;

public class TaocanBuilderA extends TaocanBuilder {

	@Override
	public void buildFood() {
		taocan.setFood("主食A");
	}

	@Override
	public void buildDrink() {
		taocan.setDrink("饮品A");
	}

	@Override
	public void buidDesserts() {
		taocan.setDesserts("甜点A");
	}

}
