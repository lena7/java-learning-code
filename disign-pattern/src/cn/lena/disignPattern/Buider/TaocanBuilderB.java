package cn.lena.disignPattern.Buider;

public class TaocanBuilderB extends TaocanBuilder {

	@Override
	public void buildFood() {
		taocan.setFood("主食B");
	}

	@Override
	public void buildDrink() {
		taocan.setDrink("饮品B");
	}

	@Override
	public void buidDesserts() {
		taocan.setDesserts("甜点B");
	}

}
