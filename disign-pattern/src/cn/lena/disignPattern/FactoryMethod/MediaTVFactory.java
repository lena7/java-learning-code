package cn.lena.disignPattern.FactoryMethod;

public class MediaTVFactory extends TVFactory {

	@Override
	TV produce() {
		return new MediaTV();
	}

}
