package cn.lena.disignPattern.FactoryMethod;

public class HaierTVFactory extends TVFactory {

	@Override
	TV produce() {
		return new HaierTV();
	}

}

