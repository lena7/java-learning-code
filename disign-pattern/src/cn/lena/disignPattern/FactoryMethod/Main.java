package cn.lena.disignPattern.FactoryMethod;

public class Main {

	public static void main(String[] args) {
		TVFactory tvFactory=new HaierTVFactory();
		TV Haier = tvFactory.produce();
		Haier.play();
		tvFactory=new MediaTVFactory();
		TV Media = tvFactory.produce();
		Media.play();
	}

}
