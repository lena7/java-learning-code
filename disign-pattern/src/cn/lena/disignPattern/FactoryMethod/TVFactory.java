package cn.lena.disignPattern.FactoryMethod;

public abstract class TVFactory {
	abstract TV produce();
}
