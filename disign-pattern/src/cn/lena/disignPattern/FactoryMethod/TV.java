package cn.lena.disignPattern.FactoryMethod;

public abstract class TV {
	abstract void play();
}
