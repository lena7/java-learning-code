package cn.lena.disignPattern.FactoryMethod;

public class NewTVFactory extends TVFactory {

	@Override
	TV produce() {
		return new NewTV();
	}

}