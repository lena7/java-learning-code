package cn.lena.disignPattern.MilkTeaQuestion;

import cn.lena.disignPattern.MilkTeaQuestion.Decorator.*;
import cn.lena.disignPattern.MilkTeaQuestion.Strategy.PromotionContext;
import org.junit.Test;

/**
 * @author lena
 * @date 2021/6/20
 * @description 测试类
 */
public class Main {

	@Test
	public void testPrice(){
		PromotionContext context = new PromotionContext();
		// 奶茶=10 原价
		Tea tea= new MilkTea();
		context.addTea(tea);
		System.out.println(context.calculatePrice());
		// 一杯奶茶=10 一杯奶茶+草莓=12 总价22 优惠后20
		Tea tea2=new Strawberry(tea);
		context.addTea(tea2);
		System.out.println(context.calculatePrice());
		// 一杯奶茶=10 一杯奶茶+草莓=12 一杯奶茶+草莓*3+芋泥+珍珠=20 总价42 优惠后33.6
		Tea tea3=new Strawberry(new Strawberry(new Strawberry(new Pearl(new Taro(tea)))));
		context.addTea(tea3);
		System.out.println(context.calculatePrice());
		// 一杯奶茶+草莓*4+芋泥*3+珍珠*3=30 单价30 打折24
		PromotionContext context2 = new PromotionContext();
		Tea tea4=new Strawberry(new Pearl(new Pearl(new Taro(new Taro(tea3)))));
		context2.addTea(tea4);
		System.out.println(context2.calculatePrice());
	}

}
