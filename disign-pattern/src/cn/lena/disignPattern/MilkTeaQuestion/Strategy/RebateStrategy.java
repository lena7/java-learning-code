package cn.lena.disignPattern.MilkTeaQuestion.Strategy;

/**
 * @author lena
 * @date 2021/6/20
 * @description 打折策略类
 */
public class RebateStrategy implements IPromotionStrategy {
	/**
	 * 满30打八折
	 * @param price
	 * @return
	 */
	@Override
	public Double calculatePrice(int price) {
		return price*0.8;
	}
}
