package cn.lena.disignPattern.MilkTeaQuestion.Strategy;

/**
 * @author lena
 * @date 2021/6/20
 * @description 满减策略类
 */
public class ReduceStrategy implements IPromotionStrategy {
	/**
	 * 满20-2
	 * @param price
	 * @return
	 */
	@Override
	public Double calculatePrice(int price) {
		return Double.valueOf(price-2);
	}
}