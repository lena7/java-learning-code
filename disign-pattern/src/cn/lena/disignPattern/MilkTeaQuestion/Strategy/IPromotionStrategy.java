package cn.lena.disignPattern.MilkTeaQuestion.Strategy;

/**
 * @author lena
 * @date 2021/6/20
 * @description 策略类接口
 */
public interface IPromotionStrategy {

	public Double calculatePrice(int price);

}
