package cn.lena.disignPattern.MilkTeaQuestion.Strategy;

/**
 * @author lena
 * @date 2021/6/20
 * @description 原价策略类
 */
public class NormalStrategy implements IPromotionStrategy {
	@Override
	public Double calculatePrice(int price) {
		return Double.valueOf(price);
	}
}
