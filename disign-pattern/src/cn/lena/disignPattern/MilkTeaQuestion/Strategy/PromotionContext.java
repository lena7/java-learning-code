package cn.lena.disignPattern.MilkTeaQuestion.Strategy;

import cn.lena.disignPattern.MilkTeaQuestion.Decorator.Tea;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lena
 * @date 2021/6/20
 * @description 策略选择
 */
public class PromotionContext {

	private IPromotionStrategy strategy;
	private List<Tea> lists;

	public void addTea(Tea tea){
		lists.add(tea);
	}

	public PromotionContext(){
		this.lists=new ArrayList<>();
	}

	public void setStrategy(IPromotionStrategy strategy) {
		this.strategy=strategy;
	}

	public Double calculatePrice() {
		int price=findStrategy();
		return strategy.calculatePrice(price);
	}

	public void clear(){
		this.lists=new ArrayList<>();
	}

	private int findStrategy(){
		int cost=0;
		for(Tea t:lists){
			cost+=t.cost();
		}
		if(cost>=30) {
			setStrategy(new RebateStrategy());
		}else if(cost>=20) {
			setStrategy(new ReduceStrategy());
		}else {
			setStrategy(new NormalStrategy());
		}
		return cost;
	}

}
