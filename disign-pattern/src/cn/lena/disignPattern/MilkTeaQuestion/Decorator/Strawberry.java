package cn.lena.disignPattern.MilkTeaQuestion.Decorator;

/**
 * @author lena
 * @date 2021/6/20
 * @description 草莓
 */
public class Strawberry extends Ingredients {

	public Strawberry(Tea tea) {
		super(tea);
	}

	public int cost(){
		return super.cost()+2;
	}

}
