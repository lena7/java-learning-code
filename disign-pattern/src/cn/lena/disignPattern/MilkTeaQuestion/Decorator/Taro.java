package cn.lena.disignPattern.MilkTeaQuestion.Decorator;

/**
 * @author lena
 * @date 2021/6/20
 * @description 芋泥
 */
public class Taro extends Ingredients {

	public Taro(Tea tea) {
		super(tea);
	}

	public int cost(){
		return super.cost()+2;
	}

}
