package cn.lena.disignPattern.MilkTeaQuestion.Decorator;

/**
 * @author lena
 * @date 2021/6/20
 * @description 珍珠
 */
public class Pearl extends Ingredients {

	public Pearl(Tea tea) {
		super(tea);
	}

	public int cost(){
		return super.cost()+2;
	}

}
