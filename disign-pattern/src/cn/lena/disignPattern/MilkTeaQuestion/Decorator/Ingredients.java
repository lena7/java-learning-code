package cn.lena.disignPattern.MilkTeaQuestion.Decorator;

/**
 * @author lena
 * @date 2021/6/20
 * @description 配料
 */
public class Ingredients extends Tea {

	private Tea tea;

	public Ingredients(Tea tea){
		this.tea=tea;
	}

	@Override
	public int cost() {
		return tea.cost();
	}
}
