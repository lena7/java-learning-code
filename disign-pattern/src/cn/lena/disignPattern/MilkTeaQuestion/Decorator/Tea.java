package cn.lena.disignPattern.MilkTeaQuestion.Decorator;

/**
 * @author lena
 * @date 2021/6/20
 */
public abstract class Tea {

	abstract public int cost();

}
