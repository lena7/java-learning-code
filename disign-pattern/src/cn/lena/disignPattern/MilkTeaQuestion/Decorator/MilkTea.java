package cn.lena.disignPattern.MilkTeaQuestion.Decorator;

/**
 * @author lena
 * @date 2021/6/20
 *
 */
public class MilkTea extends Tea {

	@Override
	public int cost() {
		return 10;
	}

}
