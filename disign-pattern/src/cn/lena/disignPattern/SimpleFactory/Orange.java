package cn.lena.disignPattern.SimpleFactory;

public class Orange extends Fruit{

	@Override
	void plant() {
		System.out.println("种了橙子");
	}

}
