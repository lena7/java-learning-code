package cn.lena.disignPattern.SimpleFactory;

public class Main {

	public static void main(String[] args) {
		FruitFactory fruitFactory = new FruitFactory();
		fruitFactory.plain("apple").plant();
		fruitFactory.plain("orange").plant();
	}

}
