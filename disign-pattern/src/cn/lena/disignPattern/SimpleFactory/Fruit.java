package cn.lena.disignPattern.SimpleFactory;

public abstract class Fruit {

	abstract void plant();

}
