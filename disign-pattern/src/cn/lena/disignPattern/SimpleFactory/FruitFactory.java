package cn.lena.disignPattern.SimpleFactory;

public class FruitFactory {		//用于根据传入值来决定创建哪一个实例

	Fruit plain(String fruit){
		if(fruit.equals("apple")){
			return new Apple();
		}else if(fruit.equals("orange")){
			return new Orange();
		}
		return null;
	}

}
