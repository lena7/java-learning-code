package cn.lena.disignPattern.SimpleFactory;

public class Apple extends Fruit{

	@Override
	void plant() {
		System.out.println("种了苹果");
	}

}
