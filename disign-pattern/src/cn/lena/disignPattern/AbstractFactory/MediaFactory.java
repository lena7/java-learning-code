package cn.lena.disignPattern.AbstractFactory;

public class MediaFactory extends Factory {
	@Override
	TV produceTV() {
		return new MediaTV();
	}

	@Override
	Frige produceFrige() {
		return new MediaFrige();
	}
}
