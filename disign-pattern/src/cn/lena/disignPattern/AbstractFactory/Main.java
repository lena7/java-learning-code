package cn.lena.disignPattern.AbstractFactory;


public class Main {
	public static void main(String[] args) {

		Factory factory1=new HaierFactory();
		TV tv1 = factory1.produceTV();
		Frige frige1 = factory1.produceFrige();
		tv1.play();
		frige1.play();
		System.out.println("-----------------");
		Factory factory2=new MediaFactory();
		TV tv2 = factory2.produceTV();
		Frige frige2 = factory2.produceFrige();
		tv2.play();
		frige2.play();

	}
}