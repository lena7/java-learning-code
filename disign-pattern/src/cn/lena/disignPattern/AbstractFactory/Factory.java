package cn.lena.disignPattern.AbstractFactory;

public abstract class Factory {

	abstract TV produceTV();

	abstract Frige produceFrige();

}
