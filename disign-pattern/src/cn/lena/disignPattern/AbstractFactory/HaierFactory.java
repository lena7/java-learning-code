package cn.lena.disignPattern.AbstractFactory;

public class HaierFactory extends Factory {
	@Override
	TV produceTV() {
		return new HaierTV();
	}

	@Override
	Frige produceFrige() {
		return new HaierFrige();
	}
}
