package cn.lena.disignPattern.AbstractFactory;

public abstract class TV {
	abstract void play();
}
