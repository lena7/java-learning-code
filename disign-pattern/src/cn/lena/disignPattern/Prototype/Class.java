package cn.lena.disignPattern.Prototype;

import java.io.*;
import java.util.ArrayList;

public class Class implements Cloneable,Serializable {

	int number;
	ArrayList<String> students;

	@Override
	protected Object clone(){
		Object object=null;
		try {
			object=super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return object;
	}

	Object deepClone1() throws IOException, ClassNotFoundException {
		ByteArrayOutputStream bao=new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bao);
		oos.writeObject(this);
		ByteArrayInputStream bis=new ByteArrayInputStream(bao.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(bis);
		return (ois.readObject());
	}

	Class deepClone2(){
		Class a=null;
		try {
			a=(Class)super.clone();
			a.students=(ArrayList)this.students.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return a;
	}


	@Override
	public String toString() {
		return getNumber()+":"+getStudents();
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public ArrayList<String> getStudents() {
		return students;
	}

	public void setStudents(ArrayList<String> students) {
		this.students = students;
	}
}
