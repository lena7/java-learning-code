package cn.lena.disignPattern.Prototype;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		Class A=new Class();
		A.setNumber(50);
		ArrayList<String> listA=new ArrayList<>();
		listA.add("A");
		A.setStudents(listA);
		//Class B=(Class)A.clone();
/*		Class B= null;
		try {
			B = (Class)A.deepClone1();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}*/
		Class B=A.deepClone2();
		System.out.println("A:"+A);
		System.out.println("B:"+B);
		System.out.println("change");
		B.setNumber(44);
		ArrayList<String> listB = B.getStudents();
		listB.set(0,"B");
		System.out.println("A:"+A);
		System.out.println("B:"+B);



/*		Email a=new Email();
		a.setSentName("A");
		ArrayList<Integer> integers = new ArrayList<>();
		integers.add(1);
		a.setList(integers);
		Email b = (Email)a.clone();
		System.out.println("原来的a："+a);
		System.out.println("原来的b："+b);
		integers.set(0,2);
		b.setSentName("B");
		System.out.println("------b浅克隆-------");
		System.out.println("现在的a："+a);
		System.out.println("修改的b："+b);
		System.out.println("\n=========================\n");
		Email c=a.deepClone1();
		System.out.println("原来的a："+a);
		System.out.println("原来的c："+c);
		c.setSentName("C");
		integers.set(0,3);
		System.out.println("------c深克隆1-------");
		System.out.println("现在的a："+a);
		System.out.println("修改的c："+c);
		System.out.println("\n=========================\n");
		Email d= null;
		try {
			d = (Email)a.deepClone2();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println("原来的a："+a);
		System.out.println("原来的d："+d);
		integers.set(0,4);
		d.setSentName("D");
		System.out.println("------d深克隆2-------");
		System.out.println("现在的a："+a);
		System.out.println("修改的d："+d);*/
	}

}
