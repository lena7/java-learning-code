package cn.lena.disignPattern.Prototype;

import java.io.*;
import java.util.ArrayList;

public class Email implements Cloneable,Serializable{

	String sentName;
	ArrayList<Integer> list;

	@Override
	public String toString() {
		return list+":"+getSentName();
	}

	@Override
	protected Object clone(){
		Object object=null;
		try {
			object=super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return object;
	}

	Email deepClone1(){
		Email a=null;
		try {
			a=(Email)super.clone();
			a.list=(ArrayList)this.list.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return a;
	}

	Object deepClone2() throws IOException, ClassNotFoundException {
		ByteArrayOutputStream bao=new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bao);
		oos.writeObject(this);
		ByteArrayInputStream bis=new ByteArrayInputStream(bao.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(bis);
		return (ois.readObject());
	}

	public ArrayList<Integer> getList() {
		return list;
	}

	public void setList(ArrayList<Integer> list) {
		this.list = list;
	}

	public String getSentName() {
		return sentName;
	}

	public void setSentName(String sentName) {
		this.sentName = sentName;
	}

}
