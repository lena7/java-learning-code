package cn.lena.disignPattern.Bridge;

public class Main {
	public static void main(String[] args) {
		Color color=new Red();
		Shape shape=new CircleShape();
		shape.setColor(color);
		shape.drawShape();
	}
}
