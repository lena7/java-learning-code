package cn.lena.disignPattern.Bridge;

public class Red implements Color {
	@Override
	public void drawShape(String shapeName) {
		System.out.println("红色的"+shapeName);
	}
}
