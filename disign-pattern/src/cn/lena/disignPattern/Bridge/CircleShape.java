package cn.lena.disignPattern.Bridge;

public class CircleShape extends Shape {
	@Override
	void drawShape() {
		this.color.drawShape("圆形");
	}
}
