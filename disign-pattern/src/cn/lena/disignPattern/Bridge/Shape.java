package cn.lena.disignPattern.Bridge;

public abstract class Shape {
	protected Color color;	//子类可以使用

	public void setColor(Color color) {
		this.color = color;
	}

	abstract void drawShape();
}
