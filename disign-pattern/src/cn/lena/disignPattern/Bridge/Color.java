package cn.lena.disignPattern.Bridge;

public interface Color {
	void drawShape(String shapeName);
}
