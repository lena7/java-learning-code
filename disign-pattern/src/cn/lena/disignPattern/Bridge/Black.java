package cn.lena.disignPattern.Bridge;

public class Black implements Color {
	@Override
	public void drawShape(String shapeName) {
		System.out.println("黑色的"+shapeName);
	}
}
