package cn.lena.disignPattern.principle;

public class ISP {

}

interface transport{
	void fix();		//修理
	void clean();	//清洗
}

interface vehicle{
	void addOil();	//加油
	void startUp();	//启动
}

class bicycle implements transport{

	@Override
	public void fix() {}

	@Override
	public void clean() {}

}
class car implements transport,vehicle{

	@Override
	public void fix() {}

	@Override
	public void clean() {}

	@Override
	public void addOil() {}

	@Override
	public void startUp() {}

}
