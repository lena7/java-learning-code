package cn.lena.disignPattern.principle;

abstract class Abstract{
	abstract void test();
}

interface AbstractTest{
	void test(Abstract ab);
}

public class DependenceInjection implements AbstractTest{

	private Abstract ab;

	//1.构造注入
	public DependenceInjection(Abstract ab){
		this.ab=ab;
	}

	//2.设值注入
	public void setAb(Abstract ab) {
		this.ab = ab;
	}

	//3.接口注入
	public void test(Abstract ab) {
		ab.test();
	}
}