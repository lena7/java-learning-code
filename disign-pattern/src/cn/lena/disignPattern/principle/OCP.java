package cn.lena.disignPattern.principle;

public class OCP {


}
abstract class fruit{
	abstract void produce();
}
class apple extends fruit{
	public void produce() {
		System.out.println("苹果");
	}
}

class orange extends fruit{
	public void produce(){
		System.out.println("橙子");
	}
}
class provider{
	void produce(fruit f){
		f.produce();
	}
}