package cn.lena.hadoop.HDFS;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import java.io.IOException;

public class testConnect {

	public static void main(String[] args) throws IOException {
		Configuration conf=new Configuration();
		//TODO:URI中的参数应该改为真实的服务器信息"hdfs://hadoop服务器名或IP:9000"
		conf.set("fs.defaultFS","hdfs://hadoop102:9000");
		//获取hdfs客户端对象
		FileSystem fs= FileSystem.get(conf);
		//在hdfs上创建路径，用于查看测试是否成功
		fs.mkdirs(new Path("/test/test"));
		//关闭资源
		fs.close();
	}
}
