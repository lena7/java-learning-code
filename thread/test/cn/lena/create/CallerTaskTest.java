package cn.lena.create;

import org.junit.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author lena
 * @date 2021/6/5
 */
public class CallerTaskTest {

	@Test
	public void testStart(){
		// 创建FutureTask对象
		FutureTask<String> futureTask=new FutureTask<>(new CallerTask());
		// 启动线程
		new Thread(futureTask).start();
		try {
			//获取线程返回值
			String result=futureTask.get();
			System.out.println(result);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

}
