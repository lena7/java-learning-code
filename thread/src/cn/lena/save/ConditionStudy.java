package cn.lena.save;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author lena
 * @date 2021/8/10
 */
public class ConditionStudy {

	public static void main(String[] args) {
		Lock lock=new ReentrantLock();
		Condition c=lock.newCondition();
	}

}
