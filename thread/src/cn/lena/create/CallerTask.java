package cn.lena.create;

import java.util.concurrent.Callable;

/**
 * @author lena
 * @date 2021/6/5
 */
public class CallerTask implements Callable<String> {

	@Override
	public String call() throws Exception {
		// 内容
		return Thread.currentThread().toString();
	}

}
