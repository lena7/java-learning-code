package cn.lena;

import org.junit.Test;

/**
 * @Author lena
 * @Date 2021/6/5
 * @decription switch传入null的情况
 */
public class SwitchParam {

	@Test
	public void testSwitchNull(){
		method(null);
	}

	public static void method(String param) {
		switch (param) {				// java.lang.NullPointerException
			// 肯定不是进入这里
			case "sth":
				System.out.println("it's sth");
				break;
			// 也不是进入这里
			case "null":
				System.out.println("it's null");
				break;
			// 也不是进入这里
			default:
				System.out.println("default");
		}
	}

}
