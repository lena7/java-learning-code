package cn.lena;

import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author lena
 * @date 2021/6/6
 */
public class TreeMapStudy {

	@Test
	public void testSave() {
		TreeMap<Integer,Integer> tm=new TreeMap<>();
		tm.put(2,3);
		tm.put(1,2);
		tm.put(6,4);
		if(tm.containsKey(1)){
			tm.put(1,tm.get(1)+3);
		}
		int[] result=new int[10];
		int k=0;
		for(Map.Entry<Integer, Integer> current:tm.entrySet()){
			if(k==result.length){
				break;
			}
			for(int i=0;i<current.getValue();i++){
				if(k==result.length){
					break;
				}
				result[k++]=current.getKey();
			}
		}
		for(int a:result){
			System.out.println(a);
		}
	}

}
