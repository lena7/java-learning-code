package cn.lena;

import org.junit.Test;

/**
 * @author lena
 * @date 2021/6/6
 * @decription try-catch-finally执行顺序
 */
public class TryCatchReturn {

	@Test
	public void test() {
		System.out.println(checkReturn4());
	}

	int checkReturn1() {
		int num=1;
		try {
			return ++num;
		} catch (Exception e) {
			return ++num;
		} finally {
			num=20;
			num=10;
		}
	}

	int checkReturn2() {
		int num=1;
		try {
			return ++num;
		} catch (Exception e) {
			return ++num;
		} finally {
			num=10;
			return ++num;
		}
	}

	int checkReturn3() {
		int num=1;
		try {
			num=1/0;
			num=100;
			return ++num;
		} catch (Exception e) {
			return num+10;
		}
	}

	int checkReturn4() {
		int num=1;
		try {
			num=1/0;
			return ++num;
		} catch (Exception e) {
			num=num+10;
			return num;
		} finally {
			num=num+100;
			return num;
		}
	}

}
