package cn.lena;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author lena
 * @date 2021/6/5
 * @decription 在for循环中删除集合元素可能带来的error
 */
public class CollectionRemove {

	@Test
	public void testIteratorRemoveCollection(){
		List<String> list = new ArrayList<>();
		list.add("1");
		list.add("2");
		Iterator<String> iterator = list.iterator();
		while (iterator.hasNext()) {
			String item = iterator.next();
			if ("1".equals(item)) {		//不会报错。
				iterator.remove();
			}
		}
		System.out.println(list);
	}

	@Test
	public void testForRemoveCollection(){
		List<String> list = new ArrayList<>();
		list.add("1");
		list.add("2");
		for (String item : list) {		// java.util.ConcurrentModificationException
			if ("2".equals(item)) {		// "1" 正确； "2" 报错。
				list.remove(item);
			}
		}
		System.out.println(list);
	}
}
