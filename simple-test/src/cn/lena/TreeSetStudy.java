package cn.lena;

import org.junit.Test;

import java.util.TreeSet;

/**
 * @author lena
 * @date 2021/6/6
 */
public class TreeSetStudy {


	/**
	 * 利用treeset存储不重复且需要有序的元素
	 */
	@Test
	public void testSave(){
		TreeSet<Integer> ts=new TreeSet();
		ts.add(700);
		ts.add(300);
		ts.add(400);
		ts.add(300);
		for(Integer a:ts){
			System.out.println(a);
		}
	}

}
