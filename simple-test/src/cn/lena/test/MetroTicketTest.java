package cn.lena.test;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;


public class MetroTicketTest extends TestCase  {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	public void testOperation_1(){
		MetroTicket obj1=new MetroTicket();
		String ExpectedResult="You have pay for the Line1. Please pick it up.";
		//String ExpectedResult="The line message is errno!!!";
		assertEquals(ExpectedResult,obj1.Operate("OneTicket","Line1",5));
	}
	public void testOperation_2(){
		MetroTicket obj1=new MetroTicket();
		String ExpectedResult="You have pay for the Line2. Please pick it up.";
		assertEquals(ExpectedResult,obj1.Operate("OneTicket","Line2",5));
	}
	public void testOperation_3(){
		MetroTicket obj1=new MetroTicket();
		String ExpectedResult="You have pay for the Line1. Please pick it up and the loose change.";
		assertEquals(ExpectedResult,obj1.Operate("OneTicket","Line1",10));
	}
	public void testOperation_4(){
		MetroTicket obj1=new MetroTicket();
		String ExpectedResult="You have pay for the Line2. Please pick it up and the loose change.";
		assertEquals(ExpectedResult,obj1.Operate("OneTicket","Line2",10));
	}
	
	public void testOperation_5(){
		MetroTicket obj1=new MetroTicket();
		obj1.Operate("OneTicket","Line1",10);
		String ExpectedResult="There has no loose change,Please pick up your money,Sorry!!!!";
		assertEquals(ExpectedResult,obj1.Operate("OneTicket","Line1",10));
	}
	public void testOperation_6(){
		MetroTicket obj1=new MetroTicket();
		obj1.Operate("OneTicket","Line2",10);
		String ExpectedResult="There has no loose change,Please pick up your money,Sorry!!!!";
		assertEquals(ExpectedResult,obj1.Operate("OneTicket","Line2",10));
	}
	
	public void testOperation_7(){
		MetroTicket obj1=new MetroTicket();
		String ExpectedResult="You have not pay enough for the month ticket. Please pay 200 RMB.";
		assertEquals(ExpectedResult,obj1.Operate("MonthTicket","Line1",10));
	}
	public void testOperation_8(){
		MetroTicket obj1=new MetroTicket();
		String ExpectedResult="You have not pay enough for the month ticket. Please pay 200 RMB.";
		assertEquals(ExpectedResult,obj1.Operate("MonthTicket","Line2",90));
	}
	public void testOperation_9(){
		MetroTicket obj1=new MetroTicket();
		String ExpectedResult="You have pay for the Line1 month ticket. Please pick it up.";
		assertEquals(ExpectedResult,obj1.Operate("MonthTicket","Line1",200));
	}
	public void testOperation_10(){
		MetroTicket obj1=new MetroTicket();
		String ExpectedResult="You have pay for the Line2 month ticket. Please pick it up.";
		assertEquals(ExpectedResult,obj1.Operate("MonthTicket","Line2",200));
	}
	public void testOperation_11(){
		MetroTicket obj1=new MetroTicket();
		String ExpectedResult="The money message is errno!!!";
		assertEquals(ExpectedResult,obj1.Operate("MonthTicket","Line1",300));
	}
	public void testOperation_12(){
		MetroTicket obj1=new MetroTicket();
		String ExpectedResult="The money message is errno!!!";
		assertEquals(ExpectedResult,obj1.Operate("MonthTicket","Line2",300));
	}
	public void testOperation_13(){
		MetroTicket obj1=new MetroTicket();
		String ExpectedResult="The money message is errno!!!";
		assertEquals(ExpectedResult,obj1.Operate("OneTicket","Line1",3));
	}
	public void testOperation_14(){
		MetroTicket obj1=new MetroTicket();
		String ExpectedResult="The money message is errno!!!";
		assertEquals(ExpectedResult,obj1.Operate("OneTicket","Line2",2));
	}

}

