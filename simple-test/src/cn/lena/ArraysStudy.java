package cn.lena;

import org.junit.Test;

import java.util.Arrays;

/**
 * @author lena
 * @date 2021/6/6
 */
public class ArraysStudy {

	@Test
	public void testCopyOf(){
		int[] a={2,4,2,6,7,3,1};
		a= Arrays.copyOf(a,3);
		for(int c:a){
			System.out.println(c);
		}
	}

}
