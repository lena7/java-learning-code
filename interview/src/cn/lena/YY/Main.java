package cn.lena.YY;

/**
 * @author lena
 * @date 2021/7/26
 */
public class Main {


	//t
	public static void main(String[] args) {
		System.out.println(new Main().nextNarcissisticNumber(42));
	}

	/**
	 * 找到比输入的整数大的下一个水仙花数
	 * @param n int整型 输入的整数
	 * @return long长整型
	 */
	public long nextNarcissisticNumber (int n) {
		int length=new String(n+"").length();
		// 对于整数位数为length的,那么n只可能由各个数的length次方组成的
		long res=findCurrent(n,length);
		while(res == -1){
			// 若在当前位数查找不到，可以查找下一位。例如输入42,在43-99查找不到水仙花数,则查找三位数的100-999
			length++;
			res=findCurrent(n,length);
		}
		return res;
	}

	/**
	 * 查找整数位数为length可能出现的水仙花数
	 * @param n
	 * @param length
	 * @return
	 */
	private long findCurrent(int n,int length){
		System.out.println(length);
		// 以这个数为起点，计算下一个水仙花数
		for(int i=n+1;i<Math.pow(10,length);i++){
			int cur=i;
			int sum=0;
			while(cur != 0) {
				int a=cur%10;
				sum += Math.pow(a,length);	// 加上当前位数的平方
				// sum的值大于当前值：不可能匹配
				if(sum > i) {
					break;
				}
				cur /= 10;
			}
			if(sum == i) {
				return i;
			}
		}
		return -1;
	}

}
