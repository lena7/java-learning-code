package cn.lena.wangyi;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author lena
 * @date 2021/8/7
 */
public class Main {

	public static void main(String[] args) {
		int[] data = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
		// 余数2的时候为X，用-1代替，方便存储和使用。以下顺序对应余数0-10
		//int[] res = {1, 0, X, 9, 8, 7, 6, 5, 4, 3, 2};
		Map<Character,Integer> yushu=new HashMap<>();
		yushu.put('0',1);yushu.put('1',0);yushu.put('X',2);
		yushu.put('9',3);yushu.put('8',4);yushu.put('7',5);
		yushu.put('6',6);yushu.put('5',7);yushu.put('4',8);
		yushu.put('3',9);yushu.put('2',10);
		Scanner sc = new Scanner(System.in);
		int count = sc.nextInt();
		while (count-- > 0) {
			String str=sc.nextLine();
			findRes(str,data,yushu);
		}
	}

	/**
	 * 2
	 * 34088119480815*66 3
	 * 520123200501169** 4
	 */
	private static void findRes(String idcard,int[] data,Map<Character,Integer> yushu) {
		// *号只会出现在第15-17位，会出现1-3次
		String[] s=idcard.split("\\*");
		if(s.length == 2) {
			// 只分成了两部分，说明只有一个*
			System.out.println(1);
			return;
		}
		// 记录*出现的位置，共出现s.length-1次
		int[] appear=new int[s.length-1];
		int curIndex=0;
		int sum=0;
		for(int i=0;i<s.length && curIndex < 17;i++) {
			String curStr=s[i];
			// 遍历当前部分的数字
			for(int j=0;j<curStr.length() && curIndex < 17;j++) {
				//System.out.println(curIndex+":"+data[curIndex] + "*" +Integer.valueOf(curStr.charAt(j)-'0'));
				sum += data[curIndex] * Integer.valueOf(curStr.charAt(j));
				curIndex++;
			}
			if(i+1 < s.length) {
				// 记录*出现的索引
				appear[i]=curIndex++;
			}
		}
		// sum求余11
		sum %= 11;
		// 最后一组数的最后一位数是余数
		String last=s[s.length-1];
		char lastChar=last.charAt(last.length()<=1?0:last.length()-1);
		// 真正的余数
		int trueYu = yushu.get(lastChar);
		System.out.println(sum);
		System.out.println(trueYu);
		// 余数-结果=系数*未知数
		System.out.println(Math.abs(sum-trueYu));
	}

	@Test
	public void test() {
		int[] data = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
		// 余数2的时候为X，用-1代替，方便存储和使用。以下顺序对应余数0-10
		//int[] res = {1, 0, X, 9, 8, 7, 6, 5, 4, 3, 2};
		Map<Character,Integer> yushu=new HashMap<>();
		yushu.put('0',1);yushu.put('1',0);yushu.put('X',2);
		yushu.put('9',3);yushu.put('8',4);yushu.put('7',5);
		yushu.put('6',6);yushu.put('5',7);yushu.put('4',8);
		yushu.put('3',9);yushu.put('2',10);
		String idcard="34088119480815*663";
		String idcard2="34088119480815**63";
		findRes(idcard2,data,yushu);
	}

}
