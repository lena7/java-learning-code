package cn.lena.aqiyi;

import org.junit.Test;

import java.util.Scanner;

/**
 * @author lena
 * @date 2021/8/2
 */
public class Main {

	// 5,6,8,26,50,48,52,55,10,1,2,1,20,5:3
	// 475.00%
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String s=sc.nextLine();
		String[] str=s.split(":");
		String[] arr=str[0].split(",");
		int size=Integer.valueOf(str[1]);
	}

	private Double findInsert(int[] arr,int size) {
		int i=0;
		Double sum=0.00;
		for(int j=0;j<size;j++) {
			sum += Integer.valueOf(arr[j+i]);
		}
		Double avg=sum/size;	// 存储当前窗口的平均值
		Double pre=0.00;	// 存储窗口上一次的平均值
		Double result=0.00;	// 结果值-增量
		Double max=0.00;
		i++;
		while(i <= arr.length-size) {
			sum=0.00;
			for(int j=0;j<size;j++) {
				sum += Integer.valueOf(arr[j+i]);
			}
			pre=avg;
			avg=sum/size;
			result=(avg - pre)/pre * 100.0;
			if(max - result < 0 && result > 0) {
				max = result;
			}
			i++;
		}
		return max;
	}

	@Test
	public void test3(){
		int size=3;
		int[] arr={5,6,8,26,50,48,52,55};
		Double d=findInsert(arr,size);
		System.out.println(String.format("%.2f",d)+"%");
	}

}
