package cn.lena.interview;

public class ThreadTest extends Thread {
	private int i=0;

	@Override
	public void run() {
		while (i!=10){
			System.out.println(i++);
		}
	}

	public static void main(String[] args) {
		Thread t=new ThreadTest();
		t.start();
	}
}
