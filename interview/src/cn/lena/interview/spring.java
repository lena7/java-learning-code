package cn.lena.interview;

import org.junit.Test;

import java.util.*;

public class spring {

	Set s=new HashSet();
	//List list=new ArrayList();
	Map map=new HashMap();

	public static void main(String[] args) {
		int[] a={3,12,4,4};
		int[] ints = Arrays.copyOf(a, 10);
		for (int i:ints) {
			System.out.print(i+",");
		}
	}


	//卓适道笔试
	/*
		学生成绩存储类
		请设计一个学生成绩存储类，这个类有三个方法
		1.保存最新批改的学生成绩（参数：成绩 返回：无）
		2.删除最新批改的学生成绩（参数：无 返回：成绩）
		3.获取最低学生成绩（参数：无 返回：最低成绩）
		要求：每个方法的时间复杂度为O(1) 请用熟悉的语言编写代码

	class studentGrade{
		private TreeSet<Double> grade;	//会自动排序
		private Double lastGrade;
		//1.保存最新批改的学生成绩（参数：成绩 返回：无）
		public void setGrade(TreeSet<Double> grade) {
			this.grade = grade;
		}
		//2.删除最新批改的学生成绩（参数：无 返回：成绩）
		public void delGrade() {
			grade.remove(lastGrade);
		}
		//3.获取最低学生成绩（参数：无 返回：最低成绩）
		public Double getMinGrade(){
			return grade.last();
		}
	} */

	/*
		3删除重复的分数
		有一组学生成绩，你直接在成绩单上删除重复出现的分数，使得每个分数只出现一次，返回删除后的分数总个数。
		不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。
		 示例 1: 给定分数列表 scores = [1,1,2], 函数应该返回新的分数个数 2, 并且原数组 scores 的前两个分数被修改为 1, 2。
		 你不需要考虑数组中超出新长度后面的元素。
		 示例 2: 给定 scores = [0,0,1,1,1,2,2,3,3,4], 函数应该返回新的分数个数 5, 并且原数组 scores 的前五个分数被修改为 0, 1, 2, 3, 4。
		 你不需要考虑数组中超出新长度后面的元素。
	 */
	public void deleteCopy(int[] scores){
		int sum=0;	//删除元素个数
		int temp=0;	//用于交换
		int current=scores[0];		//表明当前数值
		for(int i=1;i<scores.length;i++){
			if (scores[i]==current){
				sum++;	//重复个数+1
			}else{
				//scores[i]与scores[i-sum]
				temp=scores[i];
				scores[i]=scores[i-sum];
				scores[i-sum]=scores[i];
				//修改current值
				current=scores[i];
			}
		}
		//截取数组0~(length-sum-1)
		System.out.println(Arrays.toString(scores).substring(0,scores.length-sum-1));
	}
	//2统计数据
	public void sumNum(int[] arr){
		Map<Integer,Integer> map=new HashMap<>();
		for(int i=0;i<arr.length;i++){
			if(map.containsKey(arr[i])) {        //判断map是否包含该数值，包含则返回true
				map.put(arr[i], map.get(arr[i]) + 1);        //值+1
			}else{
				map.put(arr[i],1);
			}
		}
		System.out.println(map);
	}


	//美团笔试
	public void firstMei(){
		Scanner sc=new Scanner(System.in);
		int m,n;
		while(sc.hasNextInt()){
			n=sc.nextInt();
			m=sc.nextInt();
			if(m==0||n==0){
				return;
			}
			double sum=n;	//存储当前和
			double nn=n;
			//循环当前数字，不断开根号。循环结束即i=m（m次的和）
			for(int i=0;i<m-1;i++){
				nn=Math.sqrt(nn);		//n开平方根
				sum+=nn;
			}
			//截取小数点后两位
			float a=(float)Math.round(sum*100)/100;
			System.out.println(a+"");
		}
	}

	@Test 	//测试四舍五入
	public void testBaoliu(){
		float a=(float)Math.round((float) 958/25*100)/100;
		System.out.println(a);
	}
}
