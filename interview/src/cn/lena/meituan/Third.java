package cn.lena.meituan;

import java.util.*;

/**
 * @author lena
 * @date 2021/8/15
 */
public class Third {


	/**
	 * 小美在数轴上放置了若干个机器人，这些机器人每到整数时刻，就会检查是否和其他机器人重合。如果重合，它就会原地爆炸。

	 这些机器人的移动速度均为 1 。举例来说，如果一个机器人初始位于点3，然后它的方向是向右的，则时刻1会位于点4，时刻2会位于点5。

	 小美现在给小团这样一个任务：小美将给出所有机器人的初始位置和初始朝向。小团的任务是判断每个机器人的爆炸时刻。当然，如果有一些机器人永远不会爆炸，则输出-1。

	 小团向你求助。你能帮帮小团吗？

	 注意事项1：一个机器人爆炸了之后，就不会再继续存在在这个数轴上。

	 举例来说，如果有三个机器人，一个位于位置0，向右，一个位于位置2，向右，一个位于位置4，向左。则时刻1的时候，后两个机器人会在位置3相遇并发生爆炸，此后第一个机器人和第三个机器人不会在时刻2继续爆炸（因为此时已经不存在第三个机器人了）

	 注意事项2：请注意，只有整数时刻机器人才会检查重合。

	 举例来说，如果有两个机器人，一个位于位置1，向右，一个位于位置2，向左，则它们并不会在整数时刻重合。因此它们两个不存在相遇爆炸。

	 注意事项3：保证机器人初始时刻不会重叠。换句话说，不存在在时刻0就立刻爆炸的机器人。
	 */
	/**
	 * 第一行一个正整数 n 表示有 n 个机器人。
	 接下来 n 行，每行一个正整数和一个字符，以空格分隔。正整数代表机器人的坐标，字符为大写字母 L 和 R 的其中一个，分别表示机器人向左运动 和 向右运动。

	 输出出 n 行，每行一个数字，对应表示每个机器人的答案：
	 若该机器人会爆炸，输出爆炸时间；若该机器人不会爆炸，输出-1。
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		// 一共有sum个机器人
		//int sum=sc.nextInt();
		int sum=Integer.valueOf(sc.nextLine());
		TreeMap<Integer,Integer> map=new TreeMap<>();
		// 用正负表示左右 正表示向右 负表示向左
		for(int i=0;i<sum;i++) {
			String str=sc.nextLine();
			String[] s = str.split(" ");
			if(s[1] == "L") {
				// 左边是负数存储
				map.put(0-Integer.valueOf(s[0]),i);
			} else {
				// 右边是正数存储
				map.put(Integer.valueOf(s[0]),i);
			}
		}
		int[] res = findBoomTime(map);
		for(int i=0;i<res.length;i++) {
			if(res[i] == 0) {
				System.out.println("-1");
			}else{
				System.out.println(res[i]);
			}
		}
	}

	private static int[] findBoomTime(TreeMap<Integer,Integer> map) {
		int time=1;
		int[] arr=new int[map.size()];
		// 机器人全部移动一个时刻
		move(map);
		while(map.size() != 0) {
			boolean flag = isBoom(map, time++, arr);
			if(!flag && !ifBoom(map)) {
				// 当前次没有发送爆炸 且没有爆炸的可能
				return arr;
			}
		}
		return arr;
	}

	private static boolean ifBoom(TreeMap<Integer,Integer> map) {
		Set<Integer> sets = map.keySet();
		Object[] keys=sets.toArray();
		int length=keys.length;
		boolean flag=false;
		// 有一个向右一个向左 一定会相遇
		if((int)keys[0] < 0 && (int)keys[length] > 0) {
			return true;
		}
		return flag;
	}

	private static void move(TreeMap<Integer,Integer> map) {
		Set<Integer> keys = map.keySet();
		for(Integer key:keys) {
			if(key > 0) {
				map.put(key+1,map.get(key));
				map.remove(key);
			}else{
				map.put(key-1,map.get(key));
				map.remove(key);
			}
		}
	}

	private static boolean isBoom(TreeMap<Integer,Integer> map,int time,int[] arr) {
		// time表示当前是第几个时刻
		Set<Integer> sets = map.keySet();
		Object[] keys=sets.toArray();
		// 标记当前是否有爆炸元素
		boolean flag=false;
		int last=0;
		for(int i=1;i<keys.length;i++) {
			if((int)keys[i] == last) {
				// 删除
				int index=map.get(keys[i]);
				arr[index]=time;
				map.remove(keys[i]);
				flag=true;
				continue;
			}
			// treemap按照key顺序存储
			if(keys[i] == keys[i-1]) {
				// 这两个节点会爆炸
				int index = map.get(keys[i]);
				arr[index]=time;
				index=map.get(keys[i-1]);
				arr[index]=time;
				map.remove(keys[i-1]);
				last= (int) keys[i];
				flag=true;
			}
		}
		return flag;
	}

	private static int[] moveOne(int[] arr) {
		for(int i=0;i<arr.length;i++) {
			if(arr[i] > 0) {
				// 向右移动一位
				arr[i] += 1;
			} else {
				// 向左移动一位
				arr[i] -= 1;
			}
		}
		return arr;
	}

	private static int[] boomTime(int[] arr) {
		int[] res=new int[arr.length];
		// 记录剩余的机器人数
		int sum=arr.length;
		// 机器人全部移动一个时刻
		arr=moveOne(arr);
		int time=1;
		// 判断是否有爆炸机器人

		return arr;
	}


}
