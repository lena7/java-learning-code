package cn.lena.meituan;

import java.util.Scanner;

/**
 * @author lena
 * @date 2021/8/15
 */
public class Second {

	/** ac
	 小美现在有一个字符串，小美现在想知道能不能通过在字符串的尾端增加若干字符使得整个字符串变成一个回文串。
	 回文串的定义：若一个字符串，对他正序遍历和倒序遍历得到的结果是完全一致的，就称它是一个回文串。例如 abcba 就是一个回文串，因为无论正序还是倒序都是一样的。
	 对于字符串 abaaca，显然在该字符串末尾继续补上三个字符 aba 就可以构成 abaacaaba，就可以把原字符串变成回文串。换句话说，最少补上三个字符。
	 你的任务就是找到使得原来的字符串变成回文串所需要的最少字符数量。
	 本题数据保证没有空串，因此不考虑空串是否为回文串。
	 保证输入的字符串仅包含小写字母。
	 */

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String str = sc.nextLine();
		int res = minReturnStr(str);
		System.out.println(res);
	}

	/**
	 * 小美的回文串构建
	 * @param str abaaca 一行一个字符串，代表小美交给你的字符串。
	 * @return 3 (aba) 一行一个整数，表示将小美给出的字符串变成回文字符串所需要添补的最少字符数量。
	 */
	public static int minReturnStr(String str) {
		int length=str.length()-1;
		// 头尾判断
		int i=0,j=length;
		int begin=i;
		boolean flag=false;
		while(i != j && i<length && j>0) {
			if(str.charAt(i) == str.charAt(j)) {
				i++;
				j--;
				flag=true;
			} else {
				// 不相等 i退回原来位置 j退回最尾
				i=begin+1;
				j=length;
				begin=i;
				flag=false;
			}
		}
		if(flag) {
			return begin;
		}
		return length;
	}

}
