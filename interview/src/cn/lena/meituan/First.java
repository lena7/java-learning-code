package cn.lena.meituan;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author lena
 * @date 2021/8/15
 */
public class First {

	/** ac
	 * 小美给小团一个n个数字构成的数字序列，问小团能不能经过重新排列后形成1到n的排列。
	 举例：
	 小美给小团[2, 1, 3]，则可以经过重新排列后构成[1, 2, 3]，这是可行的。
	 小美给小团[4, 4, 1, 3]，则无法经过重新排列后构成[1, 2, 3, 4]，这是不可行的。
	 为了防止小团靠运气碰对答案，小美会进行多组询问。
	 */

//2
//3
//2 1 3
//4
//4 4 1 3
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		// 总共有sum组数据
		int sum=sc.nextInt();
		while(sum-- > 0) {
			// 当前组的数组长度为length
			int length=sc.nextInt();
			int[] arr=new int[length];
			for(int i=0;i<length;i++) {
				arr[i]=sc.nextInt();
			}
			boolean res = isSequence(arr);
			if(res) {
				System.out.println("Yes");
			} else {
				System.out.println("No");
			}
		}
	}

	/**
	 * 第一行是一个数T，表示有T组数据。
	 * 对于每组数据：
	 * 第一行一个数字n表示小美给出的序列由n个数字构成。
	 * 接下来一行n个空格隔开的正整数。
	 */
	public static boolean isSequence(int[] arr) {
		Arrays.sort(arr);
		if(arr[0] != 1 || arr[arr.length-1] != arr.length) {
			return false;
		}
		for(int i=1;i<arr.length;i++) {
			if(arr[i] != arr[i-1] + 1) {
				return false;
			}
		}
		return true;
	}

}
