package cn.lena.kedaxunfei;

/**
 * @author lena
 * @date 2021/8/15
 */
public class Main {

	/** 0
	 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
	 *
	 * 叶子距离
	 * 两个叶子节点间的距离为k的叶子节点对数有几个
	 * 输入：[1,2,3,4,5],3
	 * 输出：2
	 * @param root TreeNode类
	 * @param k int整型
	 * @return int整型
	 */
	public int leafPairs (TreeNode root, int k) {
		// write code here
		return 2;
	}

	class TreeNode {
		int val = 0;
		TreeNode left = null;
		TreeNode right = null;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	public static void main(String[] args) {
		System.out.println(changeNumber(10));

		//String str="abcdefghijklmnopqrstuvw???xyz";	//26
		//String str="abababcdefghijkjklmnopqrstuvwwwwxyzyz";	//31
		//String str="abababcdefghijklmnopqrstuvwwwwxyzyz";	//29
		//String str="abdefghijklmmmmmmnopqrstuvwwwwx?yz";
		//String str="wwwwabcdefghijklmnopqrstuvx?yz";	//26
		//System.out.println(findInterval(str));
	}

	/** 10
	 * 返回最短的区间长度，否则返回 -1
	 * @param str
	 * @return
	 */
	public static int findInterval (String str) {
		if(str.length() < 26) {
			return -1;
		}
		// write code here
		int wenhao=0;
		// 存储每一个字母出现的位置
		int[] flag=new int[26];
		int sum=0;
		int i=0;
		for(;i<str.length();i++) {
			if(wenhao+sum >= 26) {
				break;
			}
			char cur=str.charAt(i);
			if(cur == '?') {
				wenhao++;
				continue;
			}
			if(flag[cur-'a'] == 0) {
				sum++;
			}
			flag[cur-'a']++;
		}
		if(wenhao+sum >= 26) {
			int end=i;
			// 查看能否缩减
			i=0;
			// 当前字母出现过不止一次 可以去掉
			while(flag[str.charAt(i)-'a'] > 1) {
				flag[str.charAt(i)-'a']--;
				i++;
			}
			// 可能问号可以代替当前值
			return end-i;
		}else{
			return -1;
		}

	}

	/** ac
	 * 将num二进制中第二位0改为1
	 * @param num [1,10^9]
	 * @return
	 */
	public static int changeNumber (int num) {
		// 找到num第二个0
		int i=num;
		int sum=0;
		int twice=0;
		while(i != 0 && twice<2) {
			if(i%2 == 0) {
				// 当前有一位0
				twice++;
			}
			sum++;
			// 无符号右移一位
			i >>>= 1;
		}
		while(twice != 2) {
			sum++;
			twice++;
		}
		sum--;
		return num | 1<<sum;
	}


}
