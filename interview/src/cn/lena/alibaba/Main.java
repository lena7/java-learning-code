package cn.lena.alibaba;

import org.junit.Test;

import java.util.Scanner;

/**
 * @author lena
 * @date 2021/8/4
 */
public class Main {

	@Test
	public void test(){
		String s="231 123";
		String[] ss=s.split(" ");
		System.out.println(ss[0]+","+ss[1]);
		int max=Math.max(Integer.valueOf(ss[0]),Integer.valueOf(ss[1]));
		System.out.println(max);
		// 判断是不是质数

	}

	public static void main2(String[] arg){
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		int q=sc.nextInt();
		int[] gg=new int[n];
		// 获取第二行存入数组gg[]
		for(int i=0;i<n;i++) {
			gg[i]=sc.nextInt();
		}
		boolean flag=false;
		while(q-- >0){
			flag=false;
			// 能否在只经过1~r的星球条件下从星球l到星球r
			int l=sc.nextInt()-1;
			int r=sc.nextInt()-1;
			// 先判断能不能直接跳过去
			int res=gg[r]>gg[l]?gg[r]/gg[l]:gg[l]/gg[r];
			if(isZhiShu(res)) {
				flag=true;
			}else{
				// 查看中间是否能辅助跳过去

			}
			if(flag) {
				System.out.println("ok");
			}else{
				System.out.println("no");
			}
		}
	}

	public static boolean isZhiShu(int num){
		if(num%2 == 0 || num%3 == 0 || num%5 ==0 || num%7 == 0 || num%11 == 0 || num%13 == 0 || num%17 == 0){
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		boolean flag=false;
		// size表示组数
		int size=sc.nextInt();
		while(size-- >0) {
			flag=false;
			int num1=sc.nextInt();
			int num2=sc.nextInt();
			int max=Math.max(num1,num2);
			int sum=0;
			for(int i=0;i<30;i++) {
				int cur=sc.nextInt();
				if(sum >= 3 || flag) {
					// 不能直接break,因为要获取完30行数据,才会进入下一组
					continue;
				}
				// 当输入值为0表示用户没有输入,忽略
				if(cur != 0) {
					sum++;
					if(cur == max) {
						flag=true;
					}
				}
			}
			if(flag) {
				System.out.println("Y");
			}else{
				System.out.println("N");
			}
		}
	}

}
