package cn.lena.alibaba;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lena
 * @date 2021/6/8
 */
class Tree{
	int num;
	Tree left;
	Tree right;
	public Tree(int num){
		this.num=num;
	}
}
public class Alibaba {

	// 二叉树左视图
	public int[][] cengCi(Tree tree){
		if(tree == null) {
			return null;
		}
		//TODO:1、二叉树的取值范围，深度？
		int[][] result=new int[100][100];
		// 1.将根节点存入列表中
		result[0][0]=tree.num;
		List<Tree> lists=new ArrayList<>();
		lists.add(tree);

		int x=0,y=1;
		// 2.遍历的时候只遍历当前lists中存在的tree，就能够保证存储进去的只有下一层的结点
		// 当lists为空，表明下一层没有结点，即当前是最尾层
		while(!lists.isEmpty()){
			List<Tree> next=new ArrayList<>();
			// 遍历一层 不要操作原集合
			for(Tree t:lists) {
				if(t.left != null) {
					result[x][y++]=t.left.num;
					next.add(t.left);
				}
				if(t.right != null) {
					result[x][y++]=t.right.num;
					next.add(t.right);
				}
			}
			lists=next;
			x++;
		}
		return result;
	}

	public void printResult(int[][] result){
		if(result.length == 0) {
			// TODO:2、空树输出什么？
			return;
		}
		// result.length表明有几行，result[0].length表明有几列
		// TODO:怎么判断到了当前列数字的最末位？因为创建的时候是以最大范围创建的。
		for(int i=0;i<result[0].length;i++){
			for(int j=0;j<result.length;j++){
				System.out.print(result[i][j]);
			}
		}
	}

}
