package cn.lena.alibaba;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author lena
 * @date 2021/7/26
 */
class TreeNode{
	int val;
	TreeNode left;
	TreeNode right;
	public TreeNode(int val){
		this.val=val;
	}
}

public class leftTreeView {

	// 二叉树左视图
	public List<List<TreeNode>> leftTreeView(TreeNode root){
		Queue<TreeNode> queue=new LinkedList<>();
		List<List<TreeNode>> res=new LinkedList<>();
		queue.offer(root);
		while(queue.isEmpty()) {
			int size=queue.size();
			for(int i=0;i<size;i++) {
				TreeNode cur = queue.poll();
				List<TreeNode> list = new LinkedList<>();
				if (cur.left != null) {
					queue.offer(cur.left);
					list.add(cur.left);
				}
				if (cur.right != null) {
					queue.offer(cur.right);
					list.add(cur.right);
				}
			}
		}
		for(int i=0;i<res.size();i++){
			for(List<TreeNode> tree:res) {
				if(!tree.isEmpty()) {
					System.out.printf(tree.remove(0).val+"");
				}
			}
			System.out.println();
		}

		return res;
	}

}
