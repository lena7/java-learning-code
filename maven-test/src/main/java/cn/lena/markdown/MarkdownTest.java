package cn.lena.markdown;

import org.junit.Test;

/**
 * @author lena
 * @date 2021/8/14
 */
public class MarkdownTest {

	@Test
	public void includeHtml(){
		String markdown= "# 我是标题 \n"+" <div> hello </div>";
		String html = MarkdownUtils.markdownToHtml(markdown);
		/**
		 * 输出：
		 * <h1>我是标题</h1>
		 * <div> hello </div>
		 */
		System.out.println(html);
	}

}
