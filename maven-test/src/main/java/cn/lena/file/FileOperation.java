package cn.lena.file;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * @author lena
 * @date 2021/8/14
 */
public class FileOperation {

	@Test
	public void createFolder() {
		File f=new File("src\\file");
		// 如果文件不存在，创建文件夹
		if(!f.exists()) {
			// 创建多级目录
			f.mkdirs();
		}
	}

	@Test
	public void createFile() throws IOException {
		File f=new File("src\\file\\lena.txt");
		// 获取文件父路径：src\file
		String parent = f.getParent();
		// 父文件夹：src\file
		File parentFile = f.getParentFile();
		// 如果不存在父文件夹 创建多级目录
		if(!parentFile.exists()) {
			parentFile.mkdirs();
		}
		// 如果不存在文件 创建文件（必须要有路径才能在路径下创建文件）
		if(!f.exists()) {
			f.createNewFile();
		}
		System.out.println(f.exists());	//true
	}

	@Test	// 输出指定文件夹下后缀为java的文件
	public void outputJavaFile() {
		String path="src\\main\\java\\cn\\lena\\file";
		File f=new File(path);
		if(f.exists() && f.isDirectory()) {
			File[] files = f.listFiles();
			for(File file:files) {
				if(file.isFile() && file.getName().endsWith(".java")) {
					System.out.println(file.getPath());
				}
			}
		}
	}

}
