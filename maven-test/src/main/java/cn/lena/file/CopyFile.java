package cn.lena.file;

import java.io.*;

/**
 * @author lena
 * @date 2021/8/15
 */
public class CopyFile {

	//缓冲流一次读写一个字符串5
	public static void method5(String beforeFile,String newFile) throws IOException{
		BufferedWriter bw=new BufferedWriter(new FileWriter(newFile));
		BufferedReader br=new BufferedReader(new FileReader(beforeFile));
		String ch;
		while((ch=br.readLine())!=null){
			bw.write(ch);
			bw.newLine();
			bw.flush();
		}
		bw.close();
		br.close();
	}

	//缓冲流一次读写一个数组4
	public static void method4(String beforeFile,String newFile) throws IOException{
		BufferedWriter bw=new BufferedWriter(new FileWriter(newFile));
		BufferedReader br=new BufferedReader(new FileReader(beforeFile));
		char[] ch=new char[1048];
		int len;
		while((len=br.read(ch))!=-1)
			bw.write(ch,0,len);
		bw.close();
		br.close();
	}

	//缓冲流一次读写一个字符3
	public static void method3(String beforeFile,String newFile) throws IOException{
		BufferedWriter bw=new BufferedWriter(new FileWriter(newFile));
		BufferedReader br=new BufferedReader(new FileReader(beforeFile));
		int ch;
		while((ch=br.read())!=-1){
			bw.write(ch);
			bw.flush();
		}
		bw.close();
		br.close();
	}

	//基本流一次读写一个字符数组2
	public static void method2(String beforeFile,String newFile) throws IOException {
		FileWriter fw=new FileWriter(newFile);
		FileReader fr=new FileReader(beforeFile);
		char[] ch=new char[1048];
		int len;
		while((len=fr.read(ch))!=-1)
			fw.write(ch,0,len);
		fw.close();
		fr.close();
	}

	//基本流一次读写一个字符1
	public static void method1(String beforeFile,String newFile) throws IOException{
		FileWriter fw=new FileWriter(newFile);
		FileReader fr=new FileReader(beforeFile);
		int ch;
		while((ch=fr.read())!=-1){
			fw.write(ch);
			fw.flush();
		}
		fw.close();
		fr.close();
	}

}
