package cn.lena.file;

import org.junit.Test;

import java.io.*;

/**
 * @author lena
 * @date 2021/8/14
 */
public class FileDataOperation {

	public static void main(String[] args) throws Exception {
		File file=new File("src\\file\\lena.txt");
		testInputStream(file);
	}

	// 使用字节流读取文件
	public static void testInputStream(File file) throws Exception {
		InputStream is = new FileInputStream(file);
		if (!file.exists()) {
			System.out.println("文件不存在");
		}
		//开始读取文件
		byte[] temp_bytes = new byte[1024];
		int size = 0;  //用于记录读取文件的字节个数,如果没有读取任何的内容返回-1
		//因为文件不可能一次读取完毕，需要循环读取
		do {
			size = is.read(temp_bytes);
			if (size != -1) {
				String info = new String(temp_bytes, 0, size, "GBK");
				System.out.println("读取的内容是:" + info);
			}
		} while (size != -1);
		is.close();
	}

	@Test	// 向文件中写入内容 会清空原来的内容
	public void writeInFile() throws IOException{
		String fileName="src\\file\\lena.txt";
		BufferedWriter bw=new BufferedWriter(new FileWriter(fileName));
		String s="写入文件:hello~";
		bw.write(s);
		// 下一行
		bw.newLine();
		bw.write(s);
		// 写入文件中
		bw.flush();
		// 关闭
		bw.close();
	}

	@Test	// 读取文件内容 按行输出
	public void readFile() throws IOException{
		String fileName="src\\file\\lena.txt";
		BufferedReader br=new BufferedReader(new FileReader(fileName));
		String ch;
		while((ch=br.readLine()) != null) {
			System.out.println(ch);
		}
		br.close();
	}

}
