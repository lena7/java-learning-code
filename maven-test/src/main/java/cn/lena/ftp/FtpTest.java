package cn.lena.ftp;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author lena
 * @date 2021/8/15
 */
public class FtpTest {

	@Test
	public void uploadTest() throws IOException{
		FtpConfig ftpConfig=new FtpConfig("39.96.59.198",22,"root","leleya-2");
		FileInputStream inputStream = new FileInputStream("src\\file\\lena.txt");
		// 上传文件
		FtpUtil.upload(ftpConfig,"/root/ftpUpload",inputStream,"success.txt");
	}

}
